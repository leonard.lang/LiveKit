# First, create the variable `TESTS_SOURCES` with file paths to files that should be linked.
# The parameter `PARENT_SCOPE` exposes this variable to the parent directory.
set(TESTS_SOURCES
        ${CMAKE_CURRENT_SOURCE_DIR}/AggregatedBclParserTests.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/BclParserTests.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/BclRepresentationTests.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/BgzfBclParserTests.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/CMakeLists.txt
        ${CMAKE_CURRENT_SOURCE_DIR}/CompressedBclParserTests.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/ConfigurableTests.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/CycleManagerTests.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/FileDetectorTests.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/FilterParserTests.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/FragmentTests.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/PluginExecutionGraphTests.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/SequenceRepresentationTests.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/SerializableIndexTests.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/SerializableSequenceElementsTests.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/main.cpp
        PARENT_SCOPE)
