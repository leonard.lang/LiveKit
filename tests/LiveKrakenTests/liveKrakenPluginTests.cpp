#include "../catch2/catch.hpp"
#include "../fakeit/fakeit.hpp"
#include "../../framework/Framework.h"
#include "../../plugins/LiveKraken/liveKraken.h"

#include <iostream>

using namespace fakeit;
using namespace std;

TEST_CASE("LiveKrakenPlugin") {

    LiveKraken liveKraken;

    SECTION("Should check if the mock files have the correct type") {

        vector<string> fastQFiles;
        vector<string> fastAFiles;

        fastAFiles.emplace_back("test.fasta");
        fastAFiles.emplace_back("test2.fasta");
        fastQFiles.emplace_back("test.fastq");

        REQUIRE(liveKraken.checkFileType(fastQFiles, "fastq"));
        REQUIRE(liveKraken.checkFileType(fastAFiles, "fasta"));

        fastAFiles.emplace_back("test3.error");
        fastQFiles.emplace_back("test2.error");

        REQUIRE(!liveKraken.checkFileType(fastQFiles, "fastq"));
        REQUIRE(!liveKraken.checkFileType(fastAFiles, "fasta"));
    }

    SECTION("Should create a base string of an Read correctly") {

        Read sequence = {0b10000011, 0b10000000, 0b10000001, 0b10000010};
        string result = liveKraken.getBaseString(sequence);
        REQUIRE(result == "TACG");

        sequence = {0b00000011, 0b00000000, 0b00000001, 0b10000010};
        result = liveKraken.getBaseString(sequence);
        REQUIRE(result == "NNNG");
    }

    SECTION("Should get the correct base from a byte") {

        bitset<8> byte = 0b11111111;
        REQUIRE (liveKraken.getBaseFromByte(byte) == 3);
        byte = 0b00000000;
        REQUIRE (liveKraken.getBaseFromByte(byte) == 0);
        byte = 0b00000010;
        REQUIRE (liveKraken.getBaseFromByte(byte) == 2);
        byte = 0b00000001;
        REQUIRE (liveKraken.getBaseFromByte(byte) == 1);
    }

    SECTION("Should get the correct quality from a byte") {

        bitset<8> byte = 0b11111111;
        REQUIRE (liveKraken.getQualityFromByte(byte) == 252);
        byte = 0b00000000;
        REQUIRE (liveKraken.getQualityFromByte(byte) == 0);
        byte = 0b10101010;
        REQUIRE (liveKraken.getQualityFromByte(byte) == 168);
        byte = 0b10111111;
        REQUIRE (liveKraken.getQualityFromByte(byte) == 188);
    }

    SECTION("Should get the correct string of a sequence") {
        Read sequence = {123,12,201,0,137,236,112};

        REQUIRE(liveKraken.getBaseString(sequence) == "TACNCAA");
    }

    SECTION("Should get the correct number of bases of a FastQ-Sequence") {
        string filename = "../tests/mockFiles/LiveKrakenTestFiles/FastqFile.fastq";

        REQUIRE(liveKraken.getFastQLength(filename) == 100);
    }
}