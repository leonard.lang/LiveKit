#include "./catch2/catch.hpp"
#include "../framework/basecalls/data_representation/SequenceContainer.h"
#include "../plugins/HiLive_sharedLibraries/SequenceElement.h"
#include "../serializables/SerializableSequenceElements.h"

using namespace std;

TEST_CASE("SerializableSequenceElements") {

    vector<SequenceElement> temp;
    int mates = 0;

    for (int i=0; i<4; i++) {
        char type = (i % 2 == 0) ? 'R' : 'B'; // just to have both types
        temp.emplace_back(i, (type == 'R') ? ++mates : 0, 0, 10 + i * 10);
    }

    SerializableSequenceElements sequenceElements = SerializableSequenceElements(temp);

    SECTION("Test deserializing") {
        auto serializingResult = std::vector<char>(sequenceElements.serializableSize());
        sequenceElements.serialize(serializingResult.data());

        SerializableSequenceElements result = SerializableSequenceElements(serializingResult.data());

        for(int i=0; i<sequenceElements.getSeqs().size(); i++){
            auto originalSeq = sequenceElements.getSeqs()[i];
            auto resultSeq = result.getSeqs()[i];
            REQUIRE(originalSeq.id == resultSeq.id);
            REQUIRE(originalSeq.length == resultSeq.length);
            REQUIRE(originalSeq.mate == resultSeq.mate);
            REQUIRE(originalSeq.isBarcode() == resultSeq.isBarcode());
        }
    }
}
