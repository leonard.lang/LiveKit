#ifndef LIVEKIT_SERIALIZABLEVECTOR_H
#define LIVEKIT_SERIALIZABLEVECTOR_H

#include <vector>
#include <iostream>
#include "../framework/Serializable.h"
#include "../plugins/HiLive_sharedLibraries/SequenceElement.h"

template<typename T>
class SerializableVector : public std::vector<T>, public Serializable {
    unsigned long serializedNumElements;

    inline unsigned char getDataType() const { return 255; };

    const std::string name() const override { return "vector"; };

public:

    explicit SerializableVector(unsigned long size = 0) : std::vector<T>(size), Serializable(),
                                                          serializedNumElements(0) {};

    SerializableVector(unsigned long size, char *serializedSpace) : serializedNumElements(
            (size - serializableNameSize() - sizeof(unsigned char)) / sizeof(T)) {
        this->deserialize(serializedSpace);
    };

    explicit SerializableVector(std::vector<T> vector) : std::vector<T>(vector), Serializable(),
                                                         serializedNumElements(0) {}

    // Current supported data types: float, __int32, int, unsigned int, long, unsigned long
    void serializeData(char *serializedSpace) override {
        unsigned char dataType = getDataType();
        memcpy(serializedSpace, &dataType, sizeof(unsigned char));
        serializedSpace += sizeof(unsigned char);

        T current;
        for (unsigned long i = 0; i < this->size(); i++) {
            current = (*this)[i];
            memcpy(serializedSpace, &current, sizeof(T));
            serializedSpace += sizeof(T);
        }

        this->serializedNumElements = this->size();
    };

    void deserializeData(char *serializedSpace) override {
        serializedSpace++;
        T current;
        for (unsigned long i = 0; i < this->serializedNumElements; i++) {
            memcpy(&current, serializedSpace, sizeof(T));
            serializedSpace += sizeof(T);

            this->push_back(current);
        }

        this->serializedNumElements = 0;
    };

    void freeData() override {
        this->clear();
        this->shrink_to_fit();
    }

    unsigned long serializableDataSize() override {
        return this->size() * sizeof(T) +
               sizeof(unsigned char); // Elements + 1 Byte Data Type of Vector Elements
    };
};

template<>
inline unsigned char SerializableVector<float>::getDataType() const { return 0; }

template<>
inline unsigned char SerializableVector<int>::getDataType() const { return 1; }

template<>
inline unsigned char SerializableVector<unsigned int>::getDataType() const { return 2; }

template<>
inline unsigned char SerializableVector<long>::getDataType() const { return 3; }

template<>
inline unsigned char SerializableVector<unsigned long>::getDataType() const { return 4; }

template<>
inline unsigned char SerializableVector<SequenceElement>::getDataType() const { return 5; }

class SerializableVectorTypeLess {
public:
    static Serializable *createSerializableVector(unsigned long size, char *serializedSpace) {
        char *checkType = serializedSpace + 14;
        unsigned char type;
        memcpy(&type, checkType, sizeof(unsigned char));

        switch (type) {
            case 0:
                return new SerializableVector<float>(size, serializedSpace);
            case 1:
                return new SerializableVector<int>(size, serializedSpace);
            case 2:
                return new SerializableVector<unsigned int>(size, serializedSpace);
            case 3:
                return new SerializableVector<long>(size, serializedSpace);
            case 4:
                return new SerializableVector<unsigned long>(size, serializedSpace);
            case 5:
                return new SerializableVector<SequenceElement>(size, serializedSpace);

            default:
                throw std::runtime_error("unsupported type of vector element");
        }
    }
};

#endif //LIVEKIT_SERIALIZABLEVECTOR_H
