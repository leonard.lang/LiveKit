#ifndef LIVEKIT_SERIALIZABLEUNORDEREDMAP_H
#define LIVEKIT_SERIALIZABLEUNORDEREDMAP_H

#include <unordered_map>
#include <cstring>
#include <stdlib.h>
#include <stdio.h>
#include <cstring>
#include <iostream>
#include <utility>
#include <string>
#include <sstream>
#include "../framework/Serializable.h"
#include "SerializableIndex.h"

template<typename T, typename U>
class SerializableUnorderedMap : public std::unordered_map<T, U>, public Serializable {
    unsigned long serializedNumElements;

    inline unsigned char getDataType() const { return 255; };

    const std::string name() const override { return "umap"; };

public:

    explicit SerializableUnorderedMap(unsigned long size = 0) {
        this->serializedNumElements = 0;
    };

    int getSize() {return serializedNumElements;};

    SerializableUnorderedMap(unsigned long size, char *serializedSpace) {
        this->serializedNumElements = (size - serializableNameSize() - sizeof(unsigned char)) / sizeof(T);
        this->deserialize(serializedSpace);
    };

    void serializeData(char *serializedSpace) override {
        unsigned char dataType = getDataType();
        memcpy(serializedSpace, &dataType, sizeof(unsigned char));
        serializedSpace += sizeof(unsigned char);

        T key;
        U value;

        for(auto entry : (*this)) {
            key = entry.first;
            serializedSpace += sizeof(T);
            memcpy(serializedSpace, &key, sizeof(T));
        }

        for(auto entry : (*this)) {
            value = entry.second;
            serializedSpace += sizeof(U);
            memcpy(serializedSpace, &value, sizeof(U));
        }

        this->serializedNumElements = this->size();
    };

    void deserializeData(char *serializedSpace) override {
        serializedSpace++;
        T key;
        U value;

        std::vector<int> v;
        for (unsigned long i = 0; i < this->serializedNumElements; i++) {
            memcpy(&key, serializedSpace, sizeof(T));
            serializedSpace += sizeof(T);

            v.push_back(key);
        }
        for (unsigned long i = 0; i < this->serializedNumElements; i++) {
            memcpy(&value, serializedSpace, sizeof(U));
            serializedSpace += sizeof(U);

            (*this)[v[i]] = value;
        }

        this->serializedNumElements = 0;
    };

    void freeData() override {
        this->clear();
    }

    unsigned long serializableDataSize() override {
        return this->size() * sizeof(T) +
               sizeof(unsigned char); // Elements + 1 Byte Data Type of Map Elements
    };
};

template<>
inline unsigned char SerializableUnorderedMap<int, float>::getDataType() const { return 0; }

template<>
inline unsigned char SerializableUnorderedMap<int, int>::getDataType() const { return 1; }

template<>
inline unsigned char SerializableUnorderedMap<int, unsigned int>::getDataType() const { return 2; }

template<>
inline unsigned char SerializableUnorderedMap<int, long>::getDataType() const { return 3; }

template<>
inline unsigned char SerializableUnorderedMap<int, unsigned long>::getDataType() const { return 4; }

template<>
inline unsigned char SerializableUnorderedMap<int, SequenceElement>::getDataType() const { return 5; }

template<>
inline unsigned char SerializableUnorderedMap<unsigned long, SerializableUnorderedMap<unsigned long, unsigned long>>::getDataType() const { return 6; }


class SerializableUnorderedMapTypeLess {
public:
    static Serializable *createSerializableUnorderedMap(unsigned long size, char *serializedSpace) {
        char *checkType = serializedSpace + 12;
        unsigned char type;
        memcpy(&type, checkType, sizeof(unsigned char));

        switch (type) {
            case 0:
                return new SerializableUnorderedMap<int, float>(size, serializedSpace);
            case 1:
                return new SerializableUnorderedMap<int, int>(size, serializedSpace);
            case 2:
                return new SerializableUnorderedMap<int, unsigned int>(size, serializedSpace);
            case 3:
                return new SerializableUnorderedMap<int, long>(size, serializedSpace);
            case 4:
                return new SerializableUnorderedMap<int, unsigned long>(size, serializedSpace);
            case 5:
                return new SerializableUnorderedMap<int, SequenceElement>(size, serializedSpace);
            case 6:
                return new SerializableUnorderedMap<unsigned long, SerializableUnorderedMap<unsigned long, unsigned long>>(size, serializedSpace);
            default:
                throw std::runtime_error("unsupported type of map element");
        }
    }
};

#endif //LIVEKIT_SERIALIZABLEUNORDEREDMAP_H
