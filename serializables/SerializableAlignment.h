#ifndef LIVEKIT_SERIALIZABLEALIGNMENT_H
#define LIVEKIT_SERIALIZABLEALIGNMENT_H

#include <string>
#include "../plugins/HiLive_sharedLibraries/alnread.h"
#include "../plugins/HiLive_sharedLibraries/tools.h"
#include "../plugins/HiLive_sharedLibraries/tools_static.h"
#include "../plugins/HiLive_sharedLibraries/alnstream.h"
#include "../framework/basecalls/management/BaseManager.h"
#include "../framework/basecalls/data_representation/BclRepresentation.h"
#include "../framework/basecalls/data_representation/SequenceContainer.h"

class SerializableAlignment : public Serializable {
private:
    /** The lane to be handled. */
    uint16_t lane;

    /** The tile to be handled. */
    uint16_t tile;

    /** Total read length. */
    uint16_t rlen;

    uint16_t mate;

    uint16_t readId;

    uint32_t numReads;

    uint16_t currentReadCycle;

    bool memorySaveMode;

    bool alignmentPointersInvalid;

    /** Struct that contains all needed values that were specified in the config. */
    Configurable *settings;

    BaseManager *baseManager;

    std::vector<ReadAlignment *> alignments;

    void init_alignment_in_memory();
    void init_alignment_on_disk();

    uint64_t extend_alignment_in_memory (bool keepAlnFile, SerializableIndex *index);
    uint64_t extend_alignment_on_disk   (bool keepAlnFile, SerializableIndex *index);

    void extend_barcode_in_memory (uint16_t bc_cycle, unsigned long currentReadLength, bool keepAlnFile);
    void extend_barcode_on_disk   (uint16_t bc_cycle, unsigned long currentReadLength);

protected:
    const std::string name() const override { return "alignment"; };

public:
    SerializableAlignment(
        uint16_t ln,
        uint16_t tl,
        CountType rl,
        uint16_t mate,
        uint16_t readId,
        uint32_t numReads,
        bool memorySaveMode,
        Configurable *settings
    )   : lane(ln)
        , tile(tl)
        , rlen(rl)
        , mate(mate)
        , readId(readId)
        , numReads(numReads)
        , currentReadCycle(0)
        , memorySaveMode(memorySaveMode)
        , alignmentPointersInvalid(true)
        , settings(settings) {};

    void serializeData(char *serializedSpace) override;

    void deserializeData(char *serializedSpace) override;

    unsigned long serializableDataSize() override;

    void writeAlignFile(std::string &outputFilename);

    void freeData() override;

            void refreshAlignmentPointers();

            /**
             * Create the underlying directories of the align files.
             */
    void create_directories();

    /**
     * Initialize empty alignments for the current mate (stored as output of a virtual cycle 0).
     * @param mate Number of the current mate.
     */
    void init_alignment(Configurable *settings);

    /**
     * Extend the alignments for all reads of the specified lane and tile by one cycle.
     * @param cycle Current cycle, i.e. the cycle that will be extended.
     * @param read_no Total number of reads.
     * @param mate Number of the current mate.
     * @param index Pointer to the reference index.
     * @return Total number of seeds (for all reads).
     */
    uint64_t extend_alignment(uint16_t cycle, bool keepAlnFile, SerializableIndex *index, BaseManager *newBaseManager, Configurable *newSettings);

    /**
     * Extend the barcode for all reads with the information of the current sequencing cycle.
     * @param bc_cycle The cycle of the barcode read.
     * @param read_cycle The last handled cycle for the respective mate (should always be 0 or the full length)
     * @param read_no The number of the sequence read for which the barcode will be extended (:= index in globalAlignmentSettings.seqs).
     * @param mate The read mate to extend the barcode.
     */
    void extend_barcode(uint16_t bc_cycle, unsigned long currentReadLength, bool keepAlnFile, BaseManager *newBaseManager, Configurable *newSettings);
};

#endif //LIVEKIT_SERIALIZABLEALIGNMENT_H
