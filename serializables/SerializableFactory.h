#ifndef LIVEKIT_SERIALIZABLEFACTORY_H
#define LIVEKIT_SERIALIZABLEFACTORY_H

#include <string>
#include <map>
#include <functional>
#include "../framework/Serializable.h"

class SerializableFactory {
private:
    const static std::map<std::string, std::function<Serializable *(unsigned long, char *)>> knownSerializables;
public:
    virtual Serializable *createSerializable(const std::string &name, unsigned long size, char *serializedSpace);

    virtual void deleteSerializable(Serializable *serializable);

    virtual ~SerializableFactory() = default;
};

#endif //LIVEKIT_SERIALIZABLEFACTORY_H
