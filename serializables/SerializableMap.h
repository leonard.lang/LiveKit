#ifndef LIVEKIT_SERIALIZABLEMAP_H
#define LIVEKIT_SERIALIZABLEMAP_H

#include <map>
#include <cstring>
#include <stdlib.h>
#include <stdio.h>
#include <cstring>
#include <iostream>
#include <utility>
#include <string>
#include <sstream>
#include "../framework/Serializable.h"
#include "SerializableIndex.h"
#include "SerializableVector.h"

template<typename T, typename U>
class SerializableMap : public std::map<T, U>, public Serializable {
    unsigned long serializedNumElements;

    inline unsigned char getDataType() const { return 255; };

    const std::string name() const override { return "map"; };

public:

    explicit SerializableMap(unsigned long size = 0) {
        this->serializedNumElements = 0;
    };

    int getSize() {return serializedNumElements;};

    SerializableMap(unsigned long size, char *serializedSpace) {
        this->serializedNumElements = (size - serializableNameSize() - sizeof(unsigned char)) / sizeof(T);
        this->deserialize(serializedSpace);
    };

    void serializeData(char *serializedSpace) override {
        unsigned char dataType = getDataType();
        memcpy(serializedSpace, &dataType, sizeof(unsigned char));
        serializedSpace += sizeof(unsigned char);

        T key;
        U value;

        for(auto entry : (*this)) {
            key = entry.first;
            serializedSpace += sizeof(T);
            memcpy(serializedSpace, &key, sizeof(T));
        }

        for(auto entry : (*this)) {
            value = entry.second;
            serializedSpace += sizeof(U);
            memcpy(serializedSpace, &value, sizeof(U));
        }

        this->serializedNumElements = this->size();
    };

    void deserializeData(char *serializedSpace) override {
        serializedSpace++;
        T key;
        U value;

        std::vector<T> v;
        for (unsigned long i = 0; i < this->serializedNumElements; i++) {
            memcpy(&key, serializedSpace, sizeof(T));
            serializedSpace += sizeof(T);

            v.push_back(key);
        }
        for (unsigned long i = 0; i < this->serializedNumElements; i++) {
            memcpy(&value, serializedSpace, sizeof(U));
            serializedSpace += sizeof(U);

            (*this)[v[i]] = value;
        }

        this->serializedNumElements = 0;
    };

    void freeData() override {
        this->clear();
    }

    unsigned long serializableDataSize() override {
        return this->size() * sizeof(T) +
               sizeof(unsigned char); // Elements + 1 Byte Data Type of Map Elements
    };
};

template<>
inline unsigned char SerializableMap<int, float>::getDataType() const { return 0; }

template<>
inline unsigned char SerializableMap<int, int>::getDataType() const { return 1; }

template<>
inline unsigned char SerializableMap<int, unsigned int>::getDataType() const { return 2; }

template<>
inline unsigned char SerializableMap<int, long>::getDataType() const { return 3; }

template<>
inline unsigned char SerializableMap<int, unsigned long>::getDataType() const { return 4; }

template<>
inline unsigned char SerializableMap<int, SequenceElement>::getDataType() const { return 5; }

template<>
inline unsigned char SerializableMap<unsigned long, SerializableVector<unsigned long>>::getDataType() const { return 6; }

class SerializableMapTypeLess {
public:
    static Serializable *createSerializableMap(unsigned long size, char *serializedSpace) {
        char *checkType = serializedSpace + 11;
        unsigned char type;
        memcpy(&type, checkType, sizeof(unsigned char));

        switch (type) {
            case 0:
                return new SerializableMap<int, float>(size, serializedSpace);
            case 1:
                return new SerializableMap<int, int>(size, serializedSpace);
            case 2:
                return new SerializableMap<int, unsigned int>(size, serializedSpace);
            case 3:
                return new SerializableMap<int, long>(size, serializedSpace);
            case 4:
                return new SerializableMap<int, unsigned long>(size, serializedSpace);
            case 5:
                return new SerializableMap<int, SequenceElement>(size, serializedSpace);
            case 6:
                return new SerializableMap<unsigned long, SerializableVector<unsigned long>>(size, serializedSpace);

            default:
                throw std::runtime_error("unsupported type of map element");
        }
    }
};

#endif //LIVEKIT_SERIALIZABLEMAP_H
