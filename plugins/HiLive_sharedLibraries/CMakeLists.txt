# Create the variable `HILIVE_SOURCES` with file paths to files that should be linked.
# The parameter `PARENT_SCOPE` exposes this variable to the parent directory.
set(HILIVE_SOURCES
        ${CMAKE_CURRENT_SOURCE_DIR}/alnout.h
        ${CMAKE_CURRENT_SOURCE_DIR}/alnout.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/alnread.h
        ${CMAKE_CURRENT_SOURCE_DIR}/alnread.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/alnstream.h
        ${CMAKE_CURRENT_SOURCE_DIR}/alnstream.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/definitions.h
        ${CMAKE_CURRENT_SOURCE_DIR}/global_variables.h
        ${CMAKE_CURRENT_SOURCE_DIR}/headers.h
        ${CMAKE_CURRENT_SOURCE_DIR}/kindex.h
        ${CMAKE_CURRENT_SOURCE_DIR}/parallel.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/parallel.h
        ${CMAKE_CURRENT_SOURCE_DIR}/SequenceElement.h
        ${CMAKE_CURRENT_SOURCE_DIR}/tools.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/tools.h
        ${CMAKE_CURRENT_SOURCE_DIR}/tools_static.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/tools_static.h
        PARENT_SCOPE)
