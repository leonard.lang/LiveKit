import os
import gzip
import collections
import pickle
import argparse
import sys

def get_default_dict():
    """
    This allows us to initialize defaultdicts of defaultdicts, as types in defaultdicts must be callable
    :return: collections.defaultdict
    """

    return collections.defaultdict(int)


def calc_hit_range(position, cigar_string):
    """
    Calculates a set of reference positions covered by background reads
    :return: set(int)
    """
    startpos = position
    cigardict = evaluate_cigar_string(cigar_string)
    startpos += int(cigardict["S"])
    startpos += cigardict["H"]
    endpos = startpos + cigardict["M"]
    endpos -= cigardict["I"]
    endpos += cigardict["D"]

    return set(range(startpos, endpos))


def evaluate_cigar_string(cigar_string):
    """
    Transforms the cigar string of a read mapping into a dictionary with the letters as keys and counts as values.
    :return: Dictionary with the cigar string letters as keys and counts as values
    """

    result = collections.defaultdict(int)
    number_string = ""
    numberset = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"}
    for char in cigar_string:
        if char in numberset:
            number_string += char
        else:
            result[char] += int(number_string)
            number_string = ""
    return result


def main(path):
    datasetlist = []
    for compressedfile in os.listdir(path):
        if ".sam.gz" in path + compressedfile:
            with gzip.open(path + compressedfile, "rt") as samfile:
                datasetlist.append(collections.defaultdict(set))
                for line in samfile:
                    vals = line.split("\t")
                    datasetlist[-1][vals[2]] |= calc_hit_range(int(vals[3]), vals[5])

    resultdict = {}
    for dataset in datasetlist:
        for reference, hitpos_set in dataset.items():
            if not reference in resultdict:
                resultdict[reference] = collections.defaultdict(int)
            for hitpos in hitpos_set:
                resultdict[reference][hitpos] += 1
    outfile = open(path + "/background_coverages.pickle", "wb")
    pickle.dump(resultdict, outfile)
    outfile.close()


def parse_command_line_options(args):
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--in_directory', help='Directory with background mapping results', required=True)

    return parser.parse_args(args)


if __name__ == '__main__':
    arguments = parse_command_line_options(sys.argv[1:])
    main(path=arguments.in_directory)
