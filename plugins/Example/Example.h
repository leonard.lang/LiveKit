#ifndef LIVEKIT_EXAMPLE_H
#define LIVEKIT_EXAMPLE_H

#include <iostream>
#include "../../framework/Plugin.h"
#include "../../framework/FrameworkInterface.h"
#include "../../framework/fragments/Fragment.cpp"

class Example final : public Plugin {
public:
    FragmentContainer *runCycle(FragmentContainer *inputFragments) override;

    void init() override;

    FragmentContainer *runFullReadPostprocessing(FragmentContainer *inputFragments) override {
        (void) inputFragments; // UNUSED
        return this->framework->createNewFragmentContainer();
    };

    void finalize() override {};

    void setConfig() override;
};

#endif //LIVEKIT_EXAMPLE_H
