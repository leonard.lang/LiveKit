#include "hilive.h"

using namespace std;

mutex_map<string> fileLocks;

void Hilive::init() {
    this->t_start = time(nullptr); // Variable for runtime measurement
}

FragmentContainer *Hilive::runPreprocessing(FragmentContainer *inputFragments) {
    // Program start output
    cout
            << endl
            << "------------------------------------------------------------------" << endl
            << "HiLive v " << HiLive_VERSION_MAJOR << "." << HiLive_VERSION_MINOR
            << " PLUGIN VERSION - Realtime Alignment of Illumina Reads" << endl
            << "------------------------------------------------------------------" << endl;

    // get Index from InputFragments
    string indexName = "FMIndex";
    auto serializableIndex = (SerializableIndex *) inputFragments->get(indexName)->getSerializable(indexName);

    // Config entries should only be registered in the setConfig() method, but since we need the index for calculation
    // of the anchorLength we make an exception here
    this->registerCalculatedConfigEntry<uint16_t>("anchorLength", this->setAnchorLength(serializableIndex));
    this->registerConfigEntry<float>("softclipExtensionPenalty",
                                     float(this->getConfigEntry<uint16_t>("mismatchPenalty"))
                                     / this->getConfigEntry<uint16_t>("anchorLength"),
                                     NO_WARNING);
    this->registerConfigEntry<uint16_t>("errorRate", uint16_t(this->getConfigEntry<uint16_t>("anchorLength") / 2),
                                        NO_WARNING);
    this->registerConfigEntry<uint16_t>("seedingInterval",
                                        max((uint16_t) this->getConfigEntry<uint16_t>("anchorLength") / 2, 1),
                                        NO_WARNING);

    if (!this->getConfigEntry<string>("tempDir").empty()) {
        cout << "Temporary directory:      " << this->getConfigEntry<string>("tempDir") << endl;
    }
    if (this->getConfigEntry<OutputFormat>("outputFormat") == OutputFormat::SAM)
        cout << "SAM output directory:     " << this->getConfigEntry<string>("outDir") << endl;
    else
        cout << "BAM output directory:     " << this->getConfigEntry<string>("outDir") << endl;
    cout << "Lanes:                    ";
    //for ( uint16_t ln : globalAlignmentSettings.get_lanes() )
    //    cout << ln << " ";
    cout << endl;
    cout << "Read lengths:             ";
    string barcode_suffix;
    for (uint16_t read = 0; read != this->sequenceElements->getSeqs().size(); read++) {
        cout << this->sequenceElements->getSeqById(read).length;
        barcode_suffix = this->sequenceElements->getSeqById(read).isBarcode() ? "B" : "R";
        cout << barcode_suffix << " ";
    }
    cout << endl;
    cout << "Min. alignment score:     " << this->getConfigEntry<ScoreType>("minAs") << endl;
    cout << "Mapping mode:             "
         << to_string(this->getConfigEntry<OutputMode>("mode"), this->getConfigEntry<uint16_t>("bestN")) << endl;
    cout << "Anchor length:            " << this->getConfigEntry<uint16_t>("anchorLength") << endl;
    cout << endl;

    auto lanes = this->getConfigEntry<vector<uint16_t>>("lanes");
    auto tiles = this->getConfigEntry<vector<uint16_t>>("tiles");

    // TODO: Reimplement "continue" functionality

    // Create Alignments for all mates of all tiles of all lanes
    for (uint16_t lane : lanes)
        for (uint16_t tile : tiles)
            for (auto sequenceElement : this->sequenceElements->getSeqs()) {
                if (sequenceElement.mate == 0) continue;

                uint32_t num_reads = this->framework->getNumSequences(lane, tile);
                this->alignments[lane][tile][sequenceElement.mate] = new SerializableAlignment(
                        lane,
                        tile,
                        sequenceElement.length,
                        sequenceElement.mate,
                        sequenceElement.readId,
                        num_reads,
                        this->getConfigEntry<bool>("memorySaveMode"),
                        (Configurable *) this
                );
                this->alignments[lane][tile][sequenceElement.mate]->create_directories();
                this->alignments[lane][tile][sequenceElement.mate]->init_alignment((Configurable *) this);
            }

    return this->framework->createNewFragmentContainer();
}

/**
 * Worker function for the alignment threads.
 * @param tasks Reference to the "to do" task queue
 * @param finished Reference to the "finished" task queue
 * @param failed Reference to the "failed" task queue
 * @param surrender Control flag (threads stop if true)
 * @param index Pointer to the index object
 */
void worker(
        TaskQueue &tasks,
        atomic<CountType> &numWritingThreads,
        AlnOut *alnout,
        int outThreadCount,
        bool isKeepAlnFileCycle,
        FrameworkInterface *framework,
        Configurable *settings,
        SerializableIndex *index,
        map<uint16_t, map<uint16_t, map<uint16_t, SerializableAlignment *>>> &alignments,
        SerializableSequenceElements *sequenceElements) {
    while (tasks.size() || !alnout->is_finalized()) {

        { // scope for block guard
            atomic_increment_guard<CountType> block(numWritingThreads);

            // Start an output task if output threads and tasks available.
            if (block.get_incremented_value() <= outThreadCount)
                if (alnout->write_next() != NO_TASK)
                    continue; // If written sucessfully, check if there are more writing-tasks
        }

        Task t;

        if ((t = tasks.pop()) != NO_TASK) {
            // If "to do" task was found
            try {

                if (!t.seqEl.isBarcode()) {
                    // Seed extension if current read is sequence fragment.
                    uint64_t num_seeds = alignments[t.lane][t.tile][t.seqEl.mate]->extend_alignment(
                            t.cycle,
                            isKeepAlnFileCycle,
                            index,
                            framework->getBaseManager(),
                            settings
                    );

                    cout << "Task [" << t << "]: Found " << num_seeds << " seeds." << endl;
                } else {
                    // Barcode extension if current read is barcode fragment
                    int mateCount = framework->getMateCount();

                    for (int mate = 1; mate <= mateCount; mate++) {
                        SequenceElement seqEl = sequenceElements->getSeqByMate(mate);
                        CountType current_mate_cycle = t.seqEl.id < seqEl.id ? 0 : seqEl.length;
                        alignments[t.lane][t.tile][seqEl.mate]->extend_barcode(
                                current_mate_cycle,
                                sequenceElements->getSeqs()[t.seqEl.id].length,
                                isKeepAlnFileCycle,
                                framework->getBaseManager(),
                                settings
                        );
                    }
                    cout << "Task [" << t << "]: Extended barcode of " << mateCount << " mates." << endl;
                }

                // Make completed Task available for output-Writing
                alnout->set_task_available(
                        Task(t.lane, t.tile, framework->getCurrentCycle())
                );
            }
            catch (const exception &e) {
                cerr << "Failed to finish task [" << t << "]: " << e.what() << endl;
            }
        } else {
            // Thread is idle --> Also use it for output if the maximum number of output threads is exceeded.
            atomic_increment_guard<CountType> block(numWritingThreads);
            Task writtenTask = alnout->write_next();
            if (writtenTask == NO_TASK)
                // No Alignment Task and no output task available -> wait until an alignment task of another thread
                // finishes and, consequently, a new output task becomes available. (Sleeping prevents a Spinlock.)
                std::this_thread::sleep_for(std::chrono::milliseconds(100));
        }
    }
}

/**
 * Main function that organizes the overall structure of the program.
 * @param argc Number of arguments
 * @param argv Argument array
 * @return 0 on success, other numbers on error
 */
FragmentContainer *Hilive::runCycle(FragmentContainer *inputFragments) {
    // get Index from InputFragments
    string indexName = "FMIndex";
    auto serializableIndex = (SerializableIndex *) inputFragments->get(indexName)->getSerializable(indexName);

    auto lanes = this->getConfigEntry<vector<uint16_t>>("lanes");
    auto tiles = this->getConfigEntry<vector<uint16_t>>("tiles");

    int currentReadId = this->framework->getCurrentReadId(),
            currentMateId = this->framework->getCurrentMateId(),
            currentReadLength = this->framework->getCurrentReadLength(),
            mateCount = this->framework->getMateCount(),
            currentReadCycle = this->framework->getCurrentReadCycle(),
            currentCycle = this->framework->getCurrentCycle(),
            previousCycle = currentCycle - 1;

    bool isOutputCycle = this->isOutputCycle(currentCycle)
                         && currentCycle >= this->getConfigEntry<uint16_t>("startCycle");

    // TODO: Reimplement "continue" functionality

    // Create Tasks for all tiles of all lines for the current cycle
    TaskQueue tasks;
    for (uint16_t lane : lanes) {
        for (uint16_t tile : tiles) {
            tasks.push(Task(
                    lane,
                    tile,
                    SequenceElement(currentReadId, currentMateId, currentReadId, currentReadLength),
                    currentReadCycle
            ));
        }
    }

    AlnOut *alnout =
            isOutputCycle ? new AlnOut(lanes,
                                       tiles,
                                       currentCycle,
                                       this,
                                       this->sequenceElements,
                                       this->framework->getBaseManager(),
                                       serializableIndex,
                                       mateCount)
                          : new AlnOut(); // Null-Object

    // Number of threads currently used for writing output.
    atomic<CountType> numWritingThreads(0);

    int outThreadCount = this->getConfigEntry<int>("outThreadCount");

    auto outCycles = this->getConfigEntry<vector<uint16_t>>("outputCycles");
    auto alnFileCycles = this->getConfigEntry<vector<uint16_t>>("keepAlnFiles");
    auto memorySaveMode = this->getConfigEntry<bool>("memorySaveMode");

    // When memorySaveMode is enabled, the alnFiles of the previous cycle have to be prevented from deletion
    int alnFileCycle = memorySaveMode ? previousCycle : currentCycle;

    bool isKeepAlnFileCylce = (find(alnFileCycles.begin(), alnFileCycles.end(), alnFileCycle) != alnFileCycles.end())
                              | (find(outCycles.begin(), outCycles.end(), alnFileCycle) != outCycles.end())
                              |
                              currentReadCycle == currentReadLength; // keep the align file, for a read that is finished

    // Multi-Threaded
    vector<thread> workers;
    for (int i = 0; i < this->getConfigEntry<int>("numThreads"); i++)
        workers.emplace_back(
                worker,
                ref(tasks),
                ref(numWritingThreads),
                alnout,
                outThreadCount,
                isKeepAlnFileCylce,
                this->framework,
                (Configurable *) this,
                serializableIndex,
                ref(this->alignments),
                this->sequenceElements
        );

    for (auto &worker : workers) worker.join();

    // Single-Threaded
    //worker(tasks, numWritingThreads, alnout, outThreadCount, isKeepAlnFileCylce, this->framework, (Configurable *)this,  serializableIndex, this->alignments, this->sequenceElements);


    // TODO: Reimplement RETRY functionality of the Agenda

    delete alnout;

    return this->framework->createNewFragmentContainer();
}

bool Hilive::isOutputCycle(int cycle) {
    vector<uint16_t> outCycles = this->getConfigEntry<vector<uint16_t>>("outputCycles");
    return !(find(outCycles.begin(), outCycles.end(), cycle) == outCycles.end());
}

void Hilive::setConfig() {
    enum CompressionFormat : uint8_t {
        FORMAT_FWRITE = 0, FORMAT_GZWRITE = 1, FORMAT_LZ4WRITE = 2
    };

    this->registerConfigEntry<uint16_t>("startCycle", 32);
    this->registerConfigEntry<string>("tempDir", "./temp"); // needed for SerializableAlignment
    this->registerConfigEntry<uint64_t, string>("blockSize", "64M", this->setBlockSize());
    this->registerConfigEntry<uint8_t>("compressedFormat", FORMAT_LZ4WRITE);

    this->registerConfigEntry<uint16_t>("threadsPerTile", 1, NO_WARNING);
    this->registerConfigEntry<bool>("memorySaveMode", false, NO_WARNING);

    this->registerConfigEntry<bool>("keepAllAlnFiles", false);
    this->registerConfigEntry<vector<uint16_t>, string>("keepAlnFiles", "", [&](string input) {
        if (this->getConfigEntry<bool>("keepAllAlnFiles")) {
            vector<CountType> keep_aln_cycles(static_cast<unsigned long>(this->framework->getCycleCount()));
            iota(keep_aln_cycles.begin(), keep_aln_cycles.end(), 1);
            return keep_aln_cycles;
        } else if (!input.empty()) return toVector<uint16_t>(',')(input);
        else return vector<uint16_t>{};
    });

    this->registerConfigEntry<AlignmentMode, string>("alignMode", "balanced", [](string input) {
        char first = toupper(input[0]), second = toupper(input[1]), fifth = toupper(input[5]);
        if (first == 'B') return BALANCED;
        if (first == 'A') return ACCURATE;
        if (first == 'F') return FAST;
        if (first == 'V' && (second == 'F' || fifth == 'F')) return VERYFAST;
        if (first == 'V' && (second == 'A' || fifth == 'A')) return VERYACCURATE;
        throw runtime_error("Invalid alignment mode " + input + ".");
    });


    this->registerConfigEntry<bool>("extendedCigar", false, NO_WARNING);
    this->registerConfigEntry<uint16_t>("mismatchPenalty", 6, NO_WARNING);
    this->registerConfigEntry<float>("softclipOpeningPenalty",
                                     float(this->getConfigEntry<uint16_t>("mismatchPenalty")),
                                     NO_WARNING);
    this->registerConfigEntry<uint16_t>("insertionOpeningPenalty", 5, NO_WARNING);
    this->registerConfigEntry<uint16_t>("insertionExtensionPenalty", 3, NO_WARNING);
    this->registerConfigEntry<uint16_t>("deletionOpeningPenalty", 5, NO_WARNING);
    this->registerConfigEntry<uint16_t>("deletionExtensionPenalty", 3, NO_WARNING);
    this->registerConfigEntry<uint16_t>("matchScore", 0, NO_WARNING);

    //needed for ReadAlignment
    this->registerConfigEntry<uint16_t>("maxGapLength", 3, NO_WARNING);

    uint16_t mismatch_penalty = this->getConfigEntry<uint16_t>("mismatchPenalty")
                                + this->getConfigEntry<uint16_t>("matchScore");
    uint16_t deletion_penalty = this->getConfigEntry<uint16_t>("deletionOpeningPenalty")
                                + this->getConfigEntry<uint16_t>("deletionExtensionPenalty");
    uint16_t insertion_penalty = this->getConfigEntry<uint16_t>("insertionOpeningPenalty")
                                 + this->getConfigEntry<uint16_t>("deletionExtensionPenalty")
                                 + this->getConfigEntry<uint16_t>("matchScore");

    uint16_t maxSingleErrorPenalty = max(mismatch_penalty, max(insertion_penalty, deletion_penalty));
    this->registerConfigEntry<OutputMode, string>("mode", "ANYBEST", this->setMode());
    this->setSequenceElements(); // this sets the sequenceElements

    float default_error_rate = map<AlignmentMode, float>{
            {VERYFAST,     0.015f},
            {FAST,         0.02f},
            {BALANCED,     0.025f},
            {ACCURATE,     0.03f},
            {VERYACCURATE, 0.035f}
    }[this->getConfigEntry<AlignmentMode>("alignMode")];

    float min_as_default = (this->getConfigEntry<uint16_t>("matchScore") * (sequenceElements->getSeqByMate(1).length)) -
                           (float(sequenceElements->getSeqByMate(1).length) * default_error_rate *
                            maxSingleErrorPenalty);
    this->registerConfigEntry<ScoreType>("minAs", static_cast<const ScoreType &>(min_as_default));
    this->registerConfigEntry<bool>("keepAllSequences", false);

    // needed for AlnOut
    this->registerConfigEntry<bool, bool>("keepAllBarcodes", false, [&](bool value) {
        return this->getConfigEntry<vector<vector<string>>>("barcodeVector").empty() || value;
    });
    this->registerConfigEntry<bool>("forceResort", false);
    this->registerConfigEntry<bool>("reportUnmapped", false);
    this->registerConfigEntry<float>("maxSoftclipRatio", 0.2f);
    this->registerConfigEntry<uint16_t>("maxSoftclipLength", this->sequenceElements->getSeqByMate(1).length / 2);
    this->registerConfigEntry<OutputFormat, string>("outputFormat", "BAM", [&](string value) {
        if (value[0] == 'S') return OutputFormat::SAM;
        else if (value[0] == 'B') return OutputFormat::BAM;
        else throw runtime_error("Invalid output format: " + value + ".");
    });
    this->registerConfigEntry<vector<uint16_t>, string>("outputCycles",
                                                        to_string(this->framework->getCycleCount()),
                                                        toVector<uint16_t>(','));
    this->registerConfigEntry<int>("outThreadCount", this->getConfigEntry<int>("numThreads") / 2, NO_WARNING);

}

void Hilive::finalize() {}

function<OutputMode(string)> Hilive::setMode() {
    return [&](string value) {
        if (value.substr(0, 5) != "BESTN" && value.substr(0, 1) != "N")
            this->registerConfigEntry<uint16_t>("bestN", 0, NO_WARNING);

        if (value == "ALL" || value == "A") return OutputMode::ALL; // All hit mode
        else if (value == "UNIQUE" || value == "U") return OutputMode::UNIQUE; // Unique mode
        else if (value == "ALLBEST" || value == "H") return OutputMode::ALLBEST; // All best mode
        else if (value == "ANYBEST" || value == "B") return OutputMode::ANYBEST; // All hit mode
        else if (value.substr(0, 5) == "BESTN" || value.substr(0, 1) == "N") {
            string bestn = value.substr(0, 5) == "BESTN" ? value.substr(5) : value.substr(1);
            if (bestn.find_first_not_of("0123456789") != string::npos)
                throw runtime_error("Invalid alignment mode: " + value + ".");
            try {
                this->registerConfigEntry<uint16_t>("bestN", (uint16_t) atol(bestn.c_str()));
                return OutputMode::BESTN; // Best N scores mode
            } catch (bad_cast &ex) {
                cerr << "Error while casting length " << bestn << " to type uint16_t." << endl;
                throw ex;
            }
        } else throw runtime_error("Invalid alignment mode: " + value + "."); // Unknown mode
    };
}

void Hilive::setSequenceElements() {
    uint16_t mates = 1;
    uint16_t readId = 0;

    vector<pair<int, char>> read_structure = this->getConfigEntry<vector<pair<int, char>>>("reads");
    vector<SequenceElement> temp;

    // Iterate through input vector
    temp.reserve(read_structure.size());
    for (auto &pair : read_structure) {
        temp.emplace_back(temp.size(), (pair.second == 'R') ? mates++ : 0, readId++, pair.first);
    }

    this->sequenceElements = new SerializableSequenceElements(temp);
    this->sequenceElements->setMateCount(static_cast<uint16_t>(this->framework->getMateCount()));

    this->permanentFragment = this->framework->createNewFragment("hiliveFragment");
    this->permanentFragment->setSerializable("sequenceElements", this->sequenceElements);
}

std::function<uint64_t(std::string)> Hilive::setBlockSize() {
    return [](string input) {
        uint64_t size;
        char type = 'B';

        if (input.find_first_of("BKM") != string::npos) { // Split value to size and type
            type = *input.rbegin();
            input = input.substr(0, input.length() - 1);
        }

        if (input.find_first_not_of("0123456789") != string::npos)
            throw runtime_error("Invalid block size " + input + ". Please only use unsigned integer values.");

        try {
            size = uint64_t(atol(input.c_str()));
        } catch (bad_cast &ex) {
            cerr << "Error while casting length " << input << " to type uint16_t." << endl;
            throw ex;
        }

        if (type == 'K') return size * 1024;
        if (type == 'M') return size * 1024 * 1024;
        return size; // type == B
    };
}

std::function<uint16_t()> Hilive::setAnchorLength(SerializableIndex *index) {
    return [&, index]() {
        uint64_t genome_size = 0;
        for (uint32_t i = 0; i < index->getNumSequences(); i++) {
            genome_size += 2 * index->getSeqLengths()[i];
        }

        // relative number of reads matching a reference of given length randomly (in theory, not in biology)
        float expectation_value = .0001f;
        auto balanced_anchor_length = static_cast<uint16_t>(log(float(genome_size) / expectation_value) / log(4));

        return (uint16_t) floor(map<AlignmentMode, float>{
                {VERYFAST,     1.25f},
                {FAST,         1.125f},
                {BALANCED,     1.0f},
                {ACCURATE,     0.875f},
                {VERYACCURATE, 0.75f}
        }[this->getConfigEntry<AlignmentMode>("alignMode")] * balanced_anchor_length);
    };
}

extern "C" Hilive *create() {
    return new Hilive;
}

extern "C" void destroy(Hilive *plugin) {
    delete plugin;
}
