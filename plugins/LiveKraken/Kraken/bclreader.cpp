//
// Created by benjamin on 18.08.17.
//

#include "seqreader.hpp"
#include "krakenutil.hpp"

namespace kraken {

    BCLReader::BCLReader() {
        valid = true;
        _valid = valid;
    }

    size_t
    BCLReader::nextWorkunit(std::vector<SequenceStruct> sequencesStruct, WorkUnit &work_unit, kraken::TileInfo tile) {
        if (!_valid && concurrentBufferQueue.empty() && sequenceBuffer->seqs.empty()) {
            valid = false;
            return 0;
        }

        // fill a sequence buffer if none exists
        if (sequenceBuffer == nullptr) {
            fillSequenceBuffer(sequencesStruct, tile);
        }

        size_t total_nt = 0;

        while (!concurrentBufferQueue.empty()) {

            // get new reads from buffer and start a new reader afterwards
            if (sequenceBuffer == nullptr || sequenceBuffer->seqs.empty()) {
                // Get buffered reads.
                sequenceBuffer = move(concurrentBufferQueue.pop()); // this is blocking
            }

            total_nt = 0;

            while (!sequenceBuffer->seqs.empty()) {
                work_unit.runContainer = sequenceBuffer->runContainer;
                work_unit.seqs.emplace_back(sequenceBuffer->seqs.back());
                sequenceBuffer->seqs.pop_back();
                total_nt += work_unit.seqs.back().seq.size();
            }

            sequenceBuffer = nullptr;
        }

        std::cout << "Filling sequence buffer: " << tile.first_cycle << " .. " << tile.last_cycle << "\n";

        return total_nt;
    }

    bool BCLReader::is_valid() {
        return valid;
    }

// Fill a buffer with sequences of one tile.
// Each call of this function will advance the tile number. If all
// If all tiles in a lane are processed, will continue with tile 1 of next lane.
    bool BCLReader::fillSequenceBuffer(std::vector<SequenceStruct> sequencesStruct,
                                       TileInfo tile) {

        for (auto sequenceStruct : sequencesStruct) {
            // Create new buffer to hold the reads.
            std::unique_ptr<WorkUnit> buffer(new WorkUnit());

            if (runInfoMap[sequenceStruct.lane][sequenceStruct.tile] == nullptr)
                runInfoMap[sequenceStruct.lane][sequenceStruct.tile] = std::make_shared<RunInfoContainer>(
                        sequenceStruct.lane, sequenceStruct.tile, tile.last_cycle);

            runInfoMap[sequenceStruct.lane][sequenceStruct.tile]->lane_num = sequenceStruct.lane;
            runInfoMap[sequenceStruct.lane][sequenceStruct.tile]->tile_num = sequenceStruct.tile;
            runInfoMap[sequenceStruct.lane][sequenceStruct.tile]->processed_nt = tile.last_cycle;
            runInfoMap[sequenceStruct.lane][sequenceStruct.tile]->count = 0;


            for (int i = tile.first_cycle; i < tile.last_cycle; i++) {
                int counter = 0;
                for (auto sequence : sequenceStruct.sequences) {

                    buffer->seqs.resize(sequenceStruct.sequences.size());
                    buffer->runContainer = runInfoMap[sequenceStruct.lane][sequenceStruct.tile];

                    // Save the qualities into the read buffer.
                    buffer->seqs.at(sequenceStruct.sequences.size() - counter - 1).id = std::to_string(sequenceStruct.tile) + "_" + std::to_string(counter);
                    // TODO: insert this line in order to create a nicer output
                    // "L"+ toNDigits(sequenceStruct.lane, 3) + "_" + std::to_string(sequenceStruct.tile) + "_" + std::to_string(counter);
                    buffer->seqs.at(sequenceStruct.sequences.size() - counter - 1).seq += sequence.at(i);
                    buffer->seqs.at(sequenceStruct.sequences.size() - counter - 1).quals += "-";
                    buffer->seqs.at(sequenceStruct.sequences.size() - counter - 1).readInfo->processed_len++;
                    counter++;
                }
            }

            // Add the read buffer to the Queue which holds the precomputed buffers.
            // (It is a concurrent queue, so multiple threads can access it.)
            concurrentBufferQueue.push(move(buffer));
            buffer = nullptr;
        }

        return true;
    }
}