# LiveKraken Plugin

LiveKraken is an extension of the Kraken taxonomic sequence classification tool for classifying Illumina sequence data as it is being generated.

Visualisation 
-----
Visualisation of the results can be done using the livekraken_sankey_diagram.py script in the Visualisation folder.
Starting the program as follows will list all available options:

`./livekraken_sankey_diagram.py -h`

The script has only one required parameter `-i` that defines the LiveKraken output files to use.
It can be used several times to add several cycles of output (ideally in chronological order).

Additional options, for which sensible defaults are defined already, include:

`-c`    Used to switch from a red-green to a red-blue color scheme for the flows between nodes, default is false  
`-s`    Used to "compress" unclassified nodes by only keeping a number of reads corresponding to the sum of flows from/to nodes other than unclassified, default is false  
`-r`    Used to set on which level to bin the classified reads, valid choices are 'species', 'genus', 'family', and 'order', default is family  
`-t`    Used to determine the top x nodes to display for every cycle (plus one node serving as bin for everyting else), default is 10  
`-o`    Used to set the output directory path for the html and json file, default is "sankey"  
`-m`    Used to set the path to the names.dmp for taxonomic resolution, default is "names.dmp"  
`-n`    Used to set the path to the nodes.dmp for taxonomic resolution, default is "nodes.dmp"  

[Kraken webpage]:   http://ccb.jhu.edu/software/kraken/
[Kraken]:   http://ccb.jhu.edu/software/kraken/

Supported Sequencers
-------------
Due to differences in the structure and compression of raw sequencing data, LiveKraken currently only supports sequencers producing gz-compressed bcl (not cbcl) files. We are working on the extension of our approach for further devices.
Supported sequencers:
* Miseq
* HiSeq 1500
* HiSeq 2000/2500/3000/4000/X (untested)

Currently not supported:
* NovaSeq
* MiniSeq
* NextSeq500/550