#ifndef LIVEKIT_READPOSITIONPARSER_H
#define LIVEKIT_READPOSITIONPARSER_H

#include "../../framework/Plugin.h"
#include "../../framework/FrameworkInterface.h"
#include "../../serializables/SerializableVector.h"

/// Plugin that parses all different files that contains the position of a read
class ReadPositionParser final : public Plugin {
public:
    std::string toNDigits(int value, int N, char fill_char = '0');

    FragmentContainer *runCycle(FragmentContainer *inputFragments) override {
        (void) inputFragments; // UNUSED
        return this->framework->createNewFragmentContainer();
    };

    void init() override {};

    /** parses positions for every read out of all files */
    FragmentContainer *runPreprocessing(FragmentContainer *inputFragments) override;

    FragmentContainer *runFullReadPostprocessing(FragmentContainer *inputFragments) override { return inputFragments; };

    void finalize() override {};

    /** Set paths to the position files */
    void setConfig() override;

    /** parse the pos.txt file and write the data in a Fragment*/
    void parsePosTxtFile(std::string pathToPosFile, std::vector<uint16_t> tiles, int lane,
                         SerializableVector<float> *positionVector, bool isBgzfParser);

    /** parse the .clocs file and write the data in a Fragment
      * The clocs file format is one of 3 Illumina formats(pos, locs, and clocs) that stores position data exclusively.
      * clocs files store position data for successive clusters, compressed in bins as follows:
      *     Byte 0   : unused
      *     Byte 1-4 : unsigned int numBins
      *     The rest of the file consists of bins/blocks, where a bin consists of an integer
      *     indicating number of blocks, followed by that number of blocks and a block consists
      *     of an x-y coordinate pair.  In otherwords:
      *
      *     for each bin
      *         byte 1: Unsigned int numBlocks
      *         for each block:
      *             byte 1 : byte xRelativeCoordinate
      *             byte 2 : byte yRelativeCoordinate
      */
    void parseClocsFile(std::string pathToLocsFile, std::vector<uint16_t> tiles, int lane,
                        SerializableVector<float> *positionVector, bool isBgzfParser);

    /** parse the .locs file and write the data in a Fragment
      * The last 4 bytes of the first 12 bytes give you the # of clusters as an unsigned int.
      * The remaining sets of 8 bytes give you 2 floats, one x coordinate, one y.
      * The exact coordinates are obtained by round( 10 x [raw coordinate] + 1000)
      */
    void parseLocsFile(std::string pathToLocsFile, std::vector<uint16_t> tiles, int lane,
                       SerializableVector<float> *positionVector, bool isBgzfParser);

    /**
      * Calculate the actual x and y values with following algorithm
      *
      *     xOffset = yOffset = 0
      *     imageWidth = 2048
      *     blockSize = 25
      *     maxXbins:Int = Math.Ceiling((double)ImageWidth/(double)blockSize)
      *     for each bin:
      *         for each location:
      *             x = convert.ToSingle(xRelativeCoordinate/10f + xoffset)
      *             y = convert.toSingle(yRelativeCoordinate/10f + yoffset)
      *         if (binIndex > 0 && ((binIndex + 1) % maxXbins == 0)) {
      *            xOffset = 0; yOffset += blockSize
      *         } else xOffset += blockSize
      *
     * @param xRelativeCoordinate
     * @param yRelativeCoordinate
     * @param binCount
     * @param currentBin
     * @return Actual x and y coordinate
     */
    float *computeRealXYValue(float xRelativeCoordinate, float yRelativeCoordinate, int binCount, int currentBin);
};

#endif //LIVEKIT_READPOSITIONPARSER_H
