#ifndef LIVEKIT_PYTHONPLUGIN_H
#define LIVEKIT_PYTHONPLUGIN_H

#include <boost/python.hpp>
#include "../Plugin.h"

namespace bp = boost::python;

class PythonPlugin : public Plugin {
private:
    bp::object pluginHandle;

    bp::object import(const std::string &path);

    std::string getFileNameFromPath(std::string path);

    FragmentContainer *runPythonMethod(const char *name, FragmentContainer *inputFragments);

public:
    void setConfig() override;

    void init() override;

    FragmentContainer *runPreprocessing(FragmentContainer *inputFragments) override;

    FragmentContainer *runCycle(FragmentContainer *inputFragments) override;

    FragmentContainer *runFullReadPostprocessing(FragmentContainer *inputFragments) override;

    void finalize() override;
};


#endif //LIVEKIT_PYTHONPLUGIN_H
