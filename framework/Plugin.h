#ifndef LIVEKIT_PLUGIN_H
#define LIVEKIT_PLUGIN_H

#include "configuration/Configurable.h"
#include "fragments/Fragment.h"
#include "fragments/FragmentContainer.h"
#include "execution_planning/PluginSpecification.h"
#include "FrameworkInterface.h"

#include <exception>
#include <iostream>
#include <string>

class SequenceContainer;

class Plugin : public Configurable {
protected:

    /// pointer to the FrameworkInterface, which can be used to access framework functions
    FrameworkInterface *framework = nullptr;

    /// pointer to the permanent Fragment, which can be used to maintain local state
    std::shared_ptr<Fragment> permanentFragment = nullptr;

public:
    /// pointer to the PluginSpecification, which is used to describe inputFragments and outputFragments
    PluginSpecification *specification = nullptr;

    /**
     * This method sets the framework pointer to the current instance of the FrameworkInterface.
     * This gets called before the init() function so you can use it there.
     */
    void setFramework(FrameworkInterface *frameworkInterface) {
        this->framework = frameworkInterface;
    };

    /**
     * This method can be used to register config entries to use later on in the Plugin.
     * This is done by registerConfigEntry() and you can only use registered config entries.
     */
    void setConfig() override {};

    /**
     * This method works as a 'life cycle hook' which is called by the Framework in a defined order.
     * A plugin developer can choose to override this function and implement the respective actions which should be carried out in this step.
     * In this step the Plugin should run its initialization routine.
     * This includes setting up the permanent Fragment.
     */
    virtual void init() {};

    /**
     * This method works as a 'life cycle hook' which is called by the framework in a defined order.
     * A plugin developer can choose to override this function and implement the respective actions which should be carried out in this step.
     * This method gets called before any reads are processed.
     * This method can only access output%Fragment%s of other preprocessing steps.
     * If you want to return no Fragment%s, return an empty FragmentContainer: `this->framework->createNewFragmentContainer()`.
     * @param inputFragments pointer on a FragmentContainer containing the input Fragment%s of the Plugin. Modify the inputFragments wisely because they might be used elsewhere.
     * @return FragmentContainer containing the output Fragment%s of the Plugin
     */
    virtual FragmentContainer *runPreprocessing(FragmentContainer *inputFragments) {
        (void) inputFragments;
        return this->framework->createNewFragmentContainer();
    };

    /**
     * This method works as a 'life cycle hook' which is called by the framework in a defined order.
     * A plugin developer can choose to override this function and implement the respective actions which should be carried out in this step.
     * This method gets called every cycle and should be used for cycle-wise computations.
     * If you want to return no Fragment%s, return an empty FragmentContainer: `this->framework->createNewFragmentContainer()`.
     * @param inputFragments pointer on a FragmentCointainer containing the input Fragment%s of the Plugin. Modify the inputFragments wisely because they might be used elsewhere.
     * @return FragmentContainer containing the output Fragment%s of the Plugin
     */
    virtual FragmentContainer *runCycle(FragmentContainer *inputFragments) {
        (void) inputFragments;
        return this->framework->createNewFragmentContainer();
    };

    /**
     * This method works as a 'life cycle hook' which is called by the framework in a defined order.
     * A plugin developer can choose to override this function and implement the respective actions which should be carried out in this step.
     * This method gets called after all cycles are finished and the full read can be processed.
     * This method can still access Fragment%s of the last cycle and output%Fragment%s of other fullReadPostprocessing steps.
     * If you want to return no Fragment%s, return an empty FragmentContainer: `this->framework->createNewFragmentContainer()`.
     * @param inputFragments pointer on a FragmentContainer containing the input Fragment%s of the Plugin. Modify the inputFragments wisely because they might be used elsewhere.
     * @return FragmentContainer containing the output Fragment%s of the Plugin
     */
    virtual FragmentContainer *runFullReadPostprocessing(FragmentContainer *inputFragments) {
        (void) inputFragments;
        return this->framework->createNewFragmentContainer();
    };

    /**
     * This method works as a 'life cycle hook' which is called by the framework in a defined order.
     * A plugin developer can choose to override this function and implement the respective actions which should be carried out in this step.
     * In this step the Plugin should run its cleanup routine and finalize its activity.
     */
    virtual void finalize() {};

    virtual ~Plugin() = default;
};

#endif //LIVEKIT_PLUGIN_H
