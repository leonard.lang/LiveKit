#include <time.h>
#include <stdlib.h>
#include <map>
#include <set>
#include <vector>

#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/assign.hpp>

#include "Framework.h"

using namespace std;

Framework::Framework(const string &manifestFilePath) : frameworkInterface(this) {
    srand(time(NULL)); // required for hashing the fragments

    this->setConfigFromFile(manifestFilePath);

    string baseCallsDirectory = this->getConfigEntry<string>("BaseCallsDirectory");
    vector<uint16_t> lanes = this->getConfigEntry<vector<uint16_t>>("lanes");
    vector<uint16_t> tiles = this->getConfigEntry<vector<uint16_t>>("tiles");
    string bclParserType = this->getConfigEntry<string>("BclParserType");
    auto readStructure = this->getConfigEntry<vector<pair<int,char>>>("reads");

    this->baseManager = new BaseManager(baseCallsDirectory, lanes, tiles, bclParserType, readStructure);
    this->cycleManager = new CycleManager(readStructure);
    this->graph = new PluginExecutionGraph();

    this->loadSerializablesFactory(this->getConfigEntry<string>("SerializableFactoryPath"));

    this->setupFromManifestFile();
}

Framework::~Framework() {

    delete this->cycleManager;
    delete this->baseManager;
    delete this->graph;
}

Plugin *Framework::loadPlugin(const string &pluginPath) {
    if (this->hasEnding(pluginPath, "py"))
        return this->loadPythonPlugin();
    else
        return this->loadBinaryPlugin(pluginPath);
}

void Framework::runAllCycles() {
    FileDetector fileDetector(this->getConfigEntry<string>("BaseCallsDirectory"));

    int currentCycle, cycles = this->cycleManager->getCycleCount();
    int laneCount = this->getConfigEntry<int>("laneCount");

    fileDetector.checkForDirectory(this->getConfigEntry<string>("BaseCallsDirectory"));
    fileDetector.checkForAllLanes(laneCount);

    bool isBgzfParser = this->getConfigEntry<string>("BclParserType") == "BgzfBclParser";
    fileDetector.checkForCycleFiles(laneCount, 1, isBgzfParser);

    while ((currentCycle = this->cycleManager->getCurrentCycle()) <= cycles) {
        // checkForCycleFiles is 1 based
        fileDetector.checkForCycleFiles(laneCount, currentCycle, isBgzfParser);

        bool isFirstCycle = currentCycle == 1;

        this->baseManager->processCycle(currentCycle, isFirstCycle);

        if (isFirstCycle) this->graph->runPreprocessing();

        this->graph->runCycle();

        this->cycleManager->incrementCurrentCycle();
    }
}

void Framework::serializeAllFragments() {
    for (auto container : this->graph->preparedInputContainer) {
        for (auto &element : container->fragments) {
            element.second->serialize();
        }
    }
}

void Framework::deserializeAllFragments() {
    for (auto container : this->graph->preparedInputContainer) {
        for (auto &element : container->fragments) {
            element.second->deserialize();
        }
    }
}

void Framework::unloadPlugins() {
    this->graph->runFullReadPostprocessing();

    for (auto pluginInfo : this->pluginInfos) {
        pluginInfo.libraryInstance->finalize();
        delete pluginInfo.libraryInstance->specification;
        pluginInfo.destroy(pluginInfo.libraryInstance);
    }
}

shared_ptr<Fragment> Framework::createNewFragment(string name) {
    return shared_ptr<Fragment>(new Fragment(name, this->serializableFactory.libraryInstance));
}

FragmentContainer *Framework::createNewFragmentContainer() {
    return new FragmentContainer();
}

void Framework::serializeFragment(string name) {
    for (auto container : this->graph->preparedInputContainer) {
        if (container->fragments.count(name)) {
            container->get(name)->serialize();
            return;
        }
    }
}

void Framework::setupFromManifestFile() {
    try {
        set<Plugin *> plugins;

        // Insert plugins that can be successfully instantiated
        for (auto &pluginChild : this->parsedConfigTree.get_child("plugins")) {
            try {
                PluginSpecification spec(pluginChild.second);
                Plugin *plugin = this->setupPlugin(spec);
                plugins.insert(plugin);
            } catch (exception &e) {
                cerr << e.what() << endl;
            }
        }
        this->graph->addPlugins(plugins);
        cout << "Finished setup from manifest-file" << endl;

    } catch (exception &e) {
        cerr << "Setup of graph failed: " << e.what() << endl;
        throw (string) "Could not finish the setup from manifest-file 'framework.json': " + e.what();
    }
}

void Framework::setConfig() {
    /* Define default config options here */

    this->registerConfigEntry<int>("ramSize", 500);

    this->registerConfigEntry<string>("RunInfoPath", "");
    this->registerConfigEntry<string>("SampleSheetPath", "");
    this->registerConfigEntry<string>("BaseCallsDirectory", "");
    this->registerConfigEntry<string>("SerializableFactoryPath", "", SHOW_WARNINGS);

    this->registerConfigEntry<string>("outDir", "./out");

    this->registerConfigEntry<int>("numThreads", 1);

    this->registerConfigEntry<vector<uint16_t>, string>("barcodeErrors", "2, 2", Configurable::toVector<uint16_t>(','));

    // BEGIN: RunInfo entries
    // these are parsed from the RunInfo.xml, but overwritten by the json-Config Entries

    this->registerConfigEntry<string>("run-id", "0");
    this->registerConfigEntry<string>("flowcell-id", "Default");
    this->registerConfigEntry<string>("instrument-id", "0");
    this->registerConfigEntry<int>("surfaceCount", 1);
    this->registerConfigEntry<int>("swathCount", 1);
    this->registerConfigEntry<int>("tileCountPerSwath", 1);

    this->registerConfigEntry<vector<pair<int, char>>, string>("reads", "100R", [](string input) {
        vector<pair<int, char>> result;
        vector<string> intermediateResult;
        boost::split(intermediateResult, input, boost::is_any_of(","), boost::token_compress_on);
        for (auto el : intermediateResult) {

            if (el.substr(0, el.size() - 1).find_first_not_of("0123456789") != string::npos)
                throw runtime_error(
                        "Invalid length for read fragment " + input + ". Please only use unsigned integer values.");

            if (el[el.size() - 1] != 'B' && el[el.size() - 1] != 'R')
                throw runtime_error(
                        "'" + to_string(el[el.size() - 1])
                        + "' is no valid read type. Please use 'R' for sequencing reads or 'B' for barcode reads."
                );

            result.emplace_back(stoi(el.substr(0, el.size() - 1)), el[el.size() - 1]);
        }
        return result;
    });

    this->registerConfigEntry<vector<uint16_t>, string>("lanes", "1", Configurable::toVector<uint16_t>(','));
    this->registerConfigEntry<vector<uint16_t>, string>("tiles", "1101", Configurable::toVector<uint16_t>(','));

    this->registerConfigEntry<string>("BclParserType", "BclParser");

    this->registerConfigEntry<int>("laneCount", (int) this->getConfigEntry<vector<uint16_t>>("lanes").size());
    this->registerConfigEntry<int>("tileCount", (int) this->getConfigEntry<vector<uint16_t>>("tiles").size());

    // END: RunInfo entries

    // BEGIN: SampleSheet entries

    this->registerConfigEntry<vector<vector<string>>, string>("barcodeVector", "", Configurable::toVectorVector<string>(';',','));

    if (this->parsedConfigTree.count("SampleSheetPath")) {
        vector<string> barcodeFragmentsRead1 = sampleSheetParser.getSampleEntries("index");
        vector<string> barcodeFragmentsRead2 = sampleSheetParser.getSampleEntries("index2");
        string barcode;
        auto reads = this->getConfigEntry<vector<pair<int, char>>>("reads");

        for (int i = 0; i < (int) barcodeFragmentsRead1.size(); i++) {
            if (reads.size() == 4) {
                // If read structure contains two barcodes (RBBR)
                barcode = barcodeFragmentsRead1[i] + barcodeFragmentsRead2[i];
            } else {
                // If read structure contains only one barcode (e.g. RBR, RB)
                barcode = barcodeFragmentsRead1[i];
            }
            this->registerConfigEntry<vector<string>, string>(barcode, "N,N", Configurable::toVector<string>(','));
        }
    }
    else {
        auto barcodeVector = this->getConfigEntry<vector<vector<string>>>("barcodeVector");
        for (auto barcode : barcodeVector) {
            string concatinatedBarcode;
            for (auto fragment : barcode) {
                concatinatedBarcode += fragment;
            }
            this->registerConfigEntry<vector<string>, string>(concatinatedBarcode, "[N/A], [N/A]", Configurable::toVector<string>(','));
        }
    }
    // END: SampleSheet entries
}

Plugin *Framework::setupPlugin(PluginSpecification &spec) {
    Plugin *plugin = this->loadPlugin(spec.pluginPath);
    plugin->setFramework(&(this->frameworkInterface));

    try {
        plugin->setConfigFromFile(spec.pluginConfigPath, this->configMap);
        plugin->specification = new PluginSpecification(spec);
        plugin->init();

        cout << "Loaded plugin '" << spec.getDisplayName() << "'";
        cout << " with config-file '" << spec.pluginConfigPath << "'" << endl;

        return plugin;
    } catch (const runtime_error &e) {
        throw runtime_error("Could not setup plugin " + spec.getDisplayName());
    }
}

void Framework::setConfigFromFile(const string &configFilePath) {
    boost::property_tree::read_json(configFilePath, this->parsedConfigTree);

    // Parse xml file
    if (this->parsedConfigTree.count("RunInfoPath")){
        this->runInfoParser.parseRunInfo(this->parsedConfigTree.get<string>("RunInfoPath"));
    }

    // Parse csv file
    if (this->parsedConfigTree.count("SampleSheetPath"))
        this->sampleSheetParser.parseSampleSheet(this->parsedConfigTree.get<string>("SampleSheetPath"));

    this->setConfig();
}

Plugin *Framework::loadBinaryPlugin(const string &pluginPath) {
    PluginInfo pluginInfo;
    void *libraryHandle;
    Plugin *(*create)();
    void (*destroy)(Plugin *);

    libraryHandle = dlopen(pluginPath.c_str(), RTLD_LAZY);
    create = (Plugin *(*)()) dlsym(libraryHandle, "create");
    destroy = (void (*)(Plugin *)) dlsym(libraryHandle, "destroy");

    if (create != nullptr && destroy != nullptr) {
        Plugin *pluginInstance = create();

        pluginInfo = {destroy, pluginInstance};
        this->pluginInfos.push_back(pluginInfo);

        return pluginInstance;
    } else {
        cerr << "The plugin binary could not be located at '" << pluginPath.c_str() << "'" << endl;
        return nullptr;
    }
}

Plugin *Framework::loadPythonPlugin() {
    if (this->pythonPlugins.empty())
        PythonDependencies::initPython();

    auto pyPlugin = new PythonPlugin();
    this->pythonPlugins.push_back(pyPlugin);

    return (Plugin *) pyPlugin;
}

bool Framework::hasEnding(std::string const &fullString, std::string const &ending) {
    if (fullString.length() >= ending.length()) {
        return (0 == fullString.compare(fullString.length() - ending.length(), ending.length(), ending));
    } else {
        return false;
    }
}

FrameworkInterface *Framework::getFrameworkInterface(){
    return &this->frameworkInterface;
}

void Framework::loadSerializablesFactory(const string &libraryPath) {
    void *libraryHandle;
    SerializableFactory *(*create)();
    void (*destroy)(SerializableFactory *);

    libraryHandle = dlopen(libraryPath.c_str(), RTLD_LAZY);
    create = (SerializableFactory *(*)()) dlsym(libraryHandle, "create");
    destroy = (void (*)(SerializableFactory *)) dlsym(libraryHandle, "destroy");

    if (create != nullptr && destroy != nullptr) {
        SerializableFactory *factory = create();

        this->serializableFactory = {destroy, factory};
    } else {
        cerr << "The SerializableFactory couldn't be initialized." << endl;
        cerr << "(Please check whether the path to the shared object of the SerializableFactory is correctly specified "
                "as \"SerializableFactoryPath\"-config entry in the framework config)" << endl;
        throw runtime_error("The SerializableFactory couldn't be initialized");
    }
}
