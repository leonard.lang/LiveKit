#include "RunInfoParser.h"

#include <iostream>
#include <numeric>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/program_options/variables_map.hpp>

using namespace std;

void RunInfoParser::parseRunInfo(string runinfoFilename) {
    using boost::property_tree::ptree;

    // Parse data from a runInfo file if it exists
    ifstream f(runinfoFilename.c_str());
    if (!f.good()) {
        return;
    }

    // Load the file
    ptree tree;
    this->read_xml(tree, runinfoFilename);

    // Try to obtain the run ID
    if (tree.get_child_optional("RunInfo.Run.<xmlattr>.Id")) {
        this->runInfo_settings.insert(make_pair("run-id", tree.get<string>("RunInfo.Run.<xmlattr>.Id")));
    }

    // Try to obtain the flowcell ID
    if (tree.get_child_optional("RunInfo.Run.Flowcell")) {
        this->runInfo_settings.insert(make_pair("flowcell-id", tree.get<string>("RunInfo.Run.Flowcell")));
    }

    // Try to obtain the instrument ID
    if (tree.get_child_optional("RunInfo.Run.Instrument")) {
        this->runInfo_settings.insert(make_pair("instrument-id", tree.get<string>("RunInfo.Run.Instrument")));
    }

    // Try to obtain the read segments
    if (tree.get_child_optional("RunInfo.Run.Reads")) {

        vector<string> sequences;
        for (const auto &read : tree.get_child("RunInfo.Run.Reads")) {

            if (!read.second.get_child_optional("<xmlattr>.NumCycles")
                || !read.second.get_child_optional("<xmlattr>.IsIndexedRead")) {
                throw runtime_error("Parsing error: Read information in runInfo file is not valid.");
            }

            // Get the segments
            string sequence;
            sequence += read.second.get<string>("<xmlattr>.NumCycles");
            sequence += read.second.get<string>("<xmlattr>.IsIndexedRead") == "N" ? "R" : "B";
            sequences.push_back(sequence);
        }

        // Store the read segments
        this->runInfo_settings.insert(make_pair("reads", this->join(sequences)));
    }

    // Try to obtain the Flowcell layout
    if (tree.get_child_optional("RunInfo.Run.FlowcellLayout")) {

        auto tree_FlowcellLayout = tree.get_child("RunInfo.Run.FlowcellLayout");

        // Store the lane count
        if (tree_FlowcellLayout.get_child_optional("<xmlattr>.LaneCount")) {

            // Get the lanes as a concatenation of all lanes: 1, 2
            vector<uint16_t> lanes_vec(tree_FlowcellLayout.get<unsigned>("<xmlattr>.LaneCount"));
            iota(lanes_vec.begin(), lanes_vec.end(), 1);
            this->runInfo_settings.insert(make_pair("lanes", this->join(lanes_vec)));

            // Get the total number of lanes
            /* this->runInfo_settings.insert(make_pair("laneCount", boost::program_options::variable_value(
                    tree.get<int>("RunInfo.Run.FlowcellLayout.<xmlattr>.LaneCount"), false))); */
            // Calculated based on lanes
        }

        // Store the tile count
        if (tree_FlowcellLayout.get_child_optional("<xmlattr>.SurfaceCount")
            && tree_FlowcellLayout.get_child_optional("<xmlattr>.SwathCount")
            && tree_FlowcellLayout.get_child_optional("<xmlattr>.TileCount")) {

            if (tree_FlowcellLayout.get_child_optional("<xmlattr>.SectionPerLane")
                && tree_FlowcellLayout.get_child_optional("<xmlattr>.LanePerSection")) {
                // Concatenate tile numbers with five digits
                // This is relevant for bcl formats like BGZF where the flowcell is additionally separated into sections
                vector<uint16_t> tiles_vec = this->flowcell_layout_to_tile_numbers(
                        tree_FlowcellLayout.get<unsigned>("<xmlattr>.SurfaceCount"),
                        tree_FlowcellLayout.get<unsigned>("<xmlattr>.SwathCount"),
                        tree_FlowcellLayout.get<unsigned>("<xmlattr>.TileCount"),
                        tree_FlowcellLayout.get<unsigned>("<xmlattr>.LaneCount"),
                        tree_FlowcellLayout.get<string>("TileSet.<xmlattr>.TileNamingConvention"),
                        tree_FlowcellLayout.get<unsigned>("<xmlattr>.SectionPerLane"),
                        tree_FlowcellLayout.get<unsigned>("<xmlattr>.LanePerSection"));
                this->runInfo_settings.insert(make_pair("tiles", this->join(tiles_vec)));
            } else {
                // Concatenate tile numbers with four digits
                vector<uint16_t> tiles_vec = this->flowcell_layout_to_tile_numbers(
                        tree_FlowcellLayout.get<unsigned>("<xmlattr>.SurfaceCount"),
                        tree_FlowcellLayout.get<unsigned>("<xmlattr>.SwathCount"),
                        tree_FlowcellLayout.get<unsigned>("<xmlattr>.TileCount"));
                this->runInfo_settings.insert(make_pair("tiles", this->join(tiles_vec)));
            }

            this->runInfo_settings.insert(make_pair("surfaceCount", tree_FlowcellLayout.get<int>("<xmlattr>.SurfaceCount")));

            this->runInfo_settings.insert(make_pair("swathCount", tree_FlowcellLayout.get<int>("<xmlattr>.SwathCount")));

            this->runInfo_settings.insert(make_pair("tileCountPerSwath", tree_FlowcellLayout.get<int>("<xmlattr>.TileCount")));

            // Get the total number of tiles
            /* this->runInfo_settings.insert(make_pair("tileCount", boost::program_options::variable_value(
                    tree_FlowcellLayout.get<int>("<xmlattr>.SurfaceCount") *
                    tree_FlowcellLayout.get<int>("<xmlattr>.SwathCount") *
                    tree_FlowcellLayout.get<int>("<xmlattr>.TileCount"), false))); */
            // Calculated based on tiles
        }
    }
}

bool RunInfoParser::read_xml(boost::property_tree::ptree &xml_in, string xml_fname) {

    if (!boost::filesystem::exists(xml_fname)) {
        cout << "XML file not found: " << xml_fname << endl;
        return false;
    }

    try {
        boost::property_tree::read_xml(xml_fname, xml_in);
    } catch (const exception &ex) {
        cerr << "Error loading xml file " << xml_fname << ": " << endl << ex.what() << endl;
        return false;
    }

    return true;

}

template<typename T>
string RunInfoParser::join(std::vector<T> vector, char delim) {
    std::stringstream ss;
    for (auto el = vector.begin(); el != vector.end(); ++el) {
        ss << (*el);
        if (el != --vector.end())
            ss << delim;
    }
    return ss.str();
}
