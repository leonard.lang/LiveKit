#ifndef LIVEKIT_FRAGMENT_H
#define LIVEKIT_FRAGMENT_H

#include <map>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <cstring>
#include <iostream>
#include <utility>
#include <string>
#include <sstream>
#include "../Serializable.h"
#include "../../serializables/SerializableFactory.h"

class Fragment {
    /** A Map that stores all primitive values in pairs (key, pointer_on_primitive).
     *  A Primitive can be serialized in a standard way.
     * */
    std::map<std::string, void *> map;

    /** Serializables stores all serilizable values in pairs (key, pointer_on_Serializable).
     *  A Seriazable itself defines how it can be serialized.
     * */
    std::map<std::string, Serializable *> serializables;

    /** Stores the size of the values in 'map' in pairs (key_of_'map', size). */
    std::map<std::string, unsigned long> sizes;

    /** Individual hash of the fragment, to prevent duplicate naming on the disk by adding a hash to each name in the filename. */
    std::string hash;

    /** Set to true if the fragment is serialized, tested/set before each serialize() / deserialize(). */
    bool isSerialized = false;

    /** SerializableFactory which creates and destroys Serializables. */
    SerializableFactory *serializableFactory;

    /**
     * Serializes a string.
     * @param name The string to serialize
     * @param serializeFile The serialize file
     */
    void serializeName(const std::string &name, FILE *serializeFile);

    /**
     * Serializes a primitive data type.
     * @param primitive The primitive to serialize
     * @param size The size of the serialize file
     * @param serializeFile The serialize file
     */
    void serializePrimitive(void *primitive, unsigned long size, FILE *serializeFile);

    /**
     * Serializes a serializable.
     * @param serializable The serializable to serialize
     * @param serializeFile The serialize file
     */
    void serializeSerializable(Serializable *serializable, FILE *serializeFile);

    /**
    * Deserializes a string.
    * @param serializeFile The serialize file
    * @return The serialized string
    */
    std::string deserializeName(FILE *serializedFile);

    /**
     * Generates individual hash for the fragment, to prevent duplicate naming when creating the filename.
     * @return string
     */
    std::string generateHash();

    friend class Framework;

#ifdef TEST_DEFINITIONS

    friend class FragmentAccessHelper;

#endif

protected:

    /**
    * Constructor of fragment: Sets fragment.name to name and generates individual hash
    * @param fragmentName The name of the fragment
    */
    Fragment(std::string fragmentName, SerializableFactory *serializableFactory) : serializableFactory(
            serializableFactory), name(move(fragmentName)) {
        this->hash = generateHash();
    };

public:
    /** Name of the fragment. */
    std::string name;

    /**
    * Sets a primitive fragment component as key-value pair in 'map'.
    * If the key already exists in the map, its value is set to 'value'.
    * If the key does not exist in the map, the key-value pair is added, (key, sizeof(value)) is added in 'sizes'.
    * @param name Key of fragment component
    * @param value Value of fragment component
    */
    template<typename T>
    void set(std::string name, T value);

    /**
    * Sets a string as key-value pair in 'map'.
    * If the key already exists in the map, its value is set to 'value'.
    * If the key does not exist in the map, the key-value pair is added.
    * Internally the string is handled as a char-array.
    * @param name Key of fragment component
    * @param value Value of fragment component
    */
    void set(std::string name, std::string value);

    /**
    * Initializes array as primitive fragment component (as key-value pair in 'map').
    * If the key already exists in the map, an error is thrown.
    * If the key does not exist in the map, the array is initialized -
    * (key, array) is added in 'map', (key, sizeof(array)) is added in 'sizes'.
    * @param name Key of fragment component
    * @param size Size of the array to be initialized
    */
    template<typename T>
    T *initArray(std::string name, size_t size);

    /**
    * Gets the value of a primitive fragment component of 'name'.
    * @param name Key of primitive fragment component
    * @return primitive Fragment component
    */
    template<typename T>
    T get(std::string name);

    /**
    * Gets a pointer on a primitive fragment component of 'name'.
    * @param name Key of primitive fragment component
    * @return A pointer on primitive object of fragment component
    */
    template<typename T>
    T *getPointer(std::string name);

    /**
    * Erases key-value pair of the primitive fragment component with key 'name' out of map 'map' and out of map 'sizes'.
    * @param name Key of primitive fragment component
    */
    void erase(std::string name);

    /**
    * Sets a serializable fragment component as key-value pair in map 'serializables'.
    * If the key already exists in 'serializables', its value is set to 'value'.
    * If the key does not exist in 'serializables', the key-value pair is added.
    * @param name Key of fragment component
    * @param serializable Pointer on Serializable object of fragment component
    */
    void setSerializable(std::string name, Serializable *serializable);

    /**
    * Gets pointer on a serializable fragment component of 'name'.
    * @param name key of serializable fragment component
    * @return serializable A pointer on Serializable object of fragment component)
    */
    Serializable *getSerializable(std::string name);

    /**
     * Serializes the whole fragment (primitive and serializable components) to a file and saves it to temp directory.
     * Sets filename to name_hash.fragment in temp directory.
     * First: writes all primitive components  of 'map' into file, frees allocated memory in 'map'.
     * Second: writes all serializable components of 'serializables' into file, (serialization is implemented indiviually)
     * frees allocated memory in 'serializables', sets bool 'isSerialized' to true.
     */
    void serialize();

    /**
     * Deserializes the whole fragment (primitive and serializable components) from file in temp directory.
     * Searches filename in temp directory.
     * First: deserializes primitive components from file by adding the values to their respective key of 'map' into file,
     * frees allocated memory in 'map'.
     * Second: deserializes serializable components from file by adding the values to their respective key in 'serializables',
     * sets bool 'isSerialized' to false.
     */
    TEST_VIRTUAL void deserialize();

    /**
     * Get individual hash of fragment.
     * @return A string representing the hash.
     */
    std::string getHash();

    /**
     * Destructs Fragment.
     * Frees all memory from 'map'.
     * Deletes serialized data from the disk.
     */
    TEST_VIRTUAL ~Fragment();
};

#endif //LIVEKIT_FRAGMENT_H
