#ifndef LIVEKIT_BASEMANAGER_H
#define LIVEKIT_BASEMANAGER_H

#include "SequenceManager.h"
#include "../parser/BclParser.h"
#include "../data_representation/BclRepresentation.h"
#include "../../configuration/Configurable.h"
#include "../parser/FilterParser.h"

/**
 * The `BaseManager` bundles the interface of the `BclRepresentation` for data of newer cycles and the `SequenceManager` for complete sequences.
 * It also implements behavior to parse new BCL files and extend the `Sequences`.
 */
class BaseManager {
private:
    std::string baseCallRootDirectory;

    std::vector<uint16_t> lanes;

    std::vector<uint16_t> tiles;

    SequenceManager sequenceManager;

    BclRepresentation bclData;

    FilterRepresentation filterData;

    FilterParser filterParser;

    BclParser *bclParser;

    std::vector<std::pair<int, char>> readStructure;

public:
    BaseManager(std::string baseCallRootDirectory, std::vector<uint16_t> lanes, std::vector<uint16_t> tiles, const std::string& bclParserType, std::vector<std::pair<int, char>> readStructure);

    /**
     * Processes every new cycle by parsing the BCL and extending the sequence representation
     * @param cycleNumber The number which specifies the currentCycle starting at 0
     * @param firstCycle Flag set if it's the first cycle
     */
    virtual void processCycle(int cycleNumber, bool firstCycle);


    virtual SequenceContainer &getSequences(uint16_t lane, uint16_t tile);

    /**
     * Get a single read of a specific tile
     * @param lane The lane number starting at 0
     * @param tile The tile number starting at 0
     * @param read The read number starting at 0
     * @return Sequence Object which holds all reads of the tile
     */
    virtual Sequence &getSequence(uint16_t lane, uint16_t tile, int sequence);

    /**
     * Get a single BCL representation of a specific tile
     * @param lane The lane number starting at 0
     * @param tile The tile number starting at 0
     * @param cycle The number of the requested cycle where 0 is the newest cycle, 1 the second newest, etc.
     *              Up to maxBufferSize BCLs are accessible through this method.
     * @return BCL representation with one base for each read on the tile
     */
    virtual const BCL &getBcl(uint16_t lane, uint16_t tile, uint16_t cycle);

    /**
     * Get all buffered BCL representations of a specific tile
     * @param lane The lane number starting at 0
     * @param tile The tile number starting at 0
     * @return a deque of BCL representations which are sorted from newest to oldest
     */
    virtual const std::deque<BCL> &getBcls(uint16_t lane, uint16_t tile);

    virtual std::deque<BCL> getMostRecentBcls(uint16_t lane);

    virtual bool filterBasecall(uint16_t lane, uint16_t tile, unsigned long position);

    virtual std::vector<char>& getFilterData(uint16_t lane, uint16_t tile);

    virtual uint32_t getNumSequences(uint16_t lane, uint16_t tile);

    /**
     * Compare the sequenced barcode with the barcodeVector.
     * Does not use any baseManager data but belongs semantically to the baseManager.
     * @param barcode
     * @param barcodeVector
     * @param barcodeFaultTolerance
     * @param isSingleEnd
     * @return A sample barcode if the sequenced barcode maps on an barcodes from the barcodeVector and an empty vector if the barcode does not map on a sample barcode
     */
    virtual std::vector<char> getSampleBarcode(Read barcode, std::vector<std::vector<std::string>> barcodeVector, std::vector<uint16_t> barcodeFaultTolerance, bool isSingleEnd);

    virtual ~BaseManager();
};

#endif //LIVEKIT_BASEMANAGER_H
