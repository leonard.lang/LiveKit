#include "BgzfBclParser.h"
#include "../../utils.h"
#include <zlib.h>

using namespace std;

void BgzfBclParser::parse(uint16_t lane, uint16_t tile, uint16_t cycle, const string &bclPath) {
    // Parse files
    auto bciPath = getBciFilename(lane, bclPath);
    auto bclBgzfPath = getBgzfFilename(lane, cycle, bclPath);

    vector<char> reads = this->parseReadsByDecompressingFile(bclBgzfPath, bciPath, tile);
    BclParser::parse(lane, tile, reads);
}

/// Parses a .bci file for tile numbers and the associated number of reads.
map<uint32_t, uint32_t> BgzfBclParser::parseTileNumberToReadCount(const string &bciPath) {
    FILE *file = fopen(bciPath.c_str(), "rb");

    if (file == nullptr)
        throw runtime_error("Cannot read file: " + bciPath);

    uint16_t i = 0;
    uint32_t parsedNumber = 0;

    uint32_t tileNumber, readCount;
    map<uint32_t, uint32_t> tileNumberToReadCount;

    size_t bytesPerElement = 1, numberOfElements = 4;
    while (fread(&parsedNumber, bytesPerElement, numberOfElements, file)) {
        switch (i++ % 2) {
            case 0:
                tileNumber = parsedNumber;
                break;
            case 1:
                readCount = parsedNumber;
                tileNumberToReadCount[tileNumber] = readCount;
                break;
        }
    }

    fclose(file);
    return tileNumberToReadCount;
}

vector<char> BgzfBclParser::parseReadsByDecompressingFile(const string bclBgzfPath, const string bciPath,
                                                          uint16_t tile) {
    // Decompress bgzf file
    gzFile file = gzopen(bclBgzfPath.c_str(), "rb");

    if (file == nullptr)
        throw runtime_error("Cannot read file: " + bclBgzfPath);

    // Extract position of reads in decompressed file and readCount of tile
    auto tileNumberToReadCount = this->parseTileNumberToReadCount(bciPath);
    auto readCountsToSkip = 0;
    for (auto mapIterator = tileNumberToReadCount.begin();
         mapIterator != tileNumberToReadCount.end() && mapIterator->first != tile; ++mapIterator) {
        uint32_t tileNumber = mapIterator->first;
        readCountsToSkip += tileNumberToReadCount.at(tileNumber);
    }
    uint32_t readCount = tileNumberToReadCount.at(tile);

    // Seek until beginning of file
    gzseek(file, 0, SEEK_SET);

    // Add number of reads to base call vector
    vector<char> baseCalls;
    baseCalls.reserve(sizeof(readCount) + readCount);

    baseCalls.push_back(readCount >> 0);
    baseCalls.push_back(readCount >> 8);
    baseCalls.push_back(readCount >> 16);
    baseCalls.push_back(readCount >> 24);

    // Skip total number of reads and seek to base calls of tile
    gzseek(file, 4 + readCountsToSkip, SEEK_CUR);

    // Extract base calls of tile
    for (int i = 0; i < readCount; i++) {
        uint8_t parsedRead = 0;
        gzread(file, &parsedRead, sizeof(parsedRead));
        baseCalls.push_back(parsedRead);
    }

    gzclose(file);
    return baseCalls;
}
