#ifndef LIVEKIT_BCLREPRESENTATION_H
#define LIVEKIT_BCLREPRESENTATION_H

#include <map>
#include <vector>
#include <deque>

/**
 * Container for all bases and qualities from a single tile and a single cycle
 */
typedef std::vector<char> BCL;
/**
 * Buffer for `BCL` data structured after lane, tile, cycle, where cycle = 0 is the newest cycle.
 * The maximum deque size is `bufferSize`.
 */
typedef std::map<uint16_t, std::map<uint16_t, std::deque<BCL>>> BCLDATA;

/**
 * The BclRepresentation holds all buffered `BCL`s and exposes a public interface for interaction.
 */
class BclRepresentation {
private:
    /**
     * Stores the parsed BCL data for each tile.
     * Only the most recent BCLs are stored to save memory
     * usage: bclData[lane][tile][cycleOffset]
     * cycleOffset = 0 means the most recent parsed BCL
     */
    BCLDATA bclData;

    /**
     * Specifies how many BCLs are stored for each tile
     * The size of the buffer is limited because the whole BCL data is stored redundant in SequenceContainers
     */
    uint16_t bufferSize;

public:
    /**
     * Creates a BclRepresentation and initializes the bclData which stores the actual BCLs
     * @param lanes
     * @param tiles
     * @param bufferSize specifies how many cycles we keep in memory
     */
    BclRepresentation(std::vector<uint16_t> &lanes, std::vector<uint16_t> &tiles, uint16_t bufferSize = 10);

    /**
     * @param lane
     * @param tile
     * @return the number of parsed nucletides for the specified lane
     */
    uint32_t getBclSize(uint16_t lane, uint16_t tile);

    /**
     * Changes the amount of BCLs that are stored
     * If the old bufferSize was 10 and it is then set to 20, this does not mean that the BclRepresentation now restores
     * the additional 10 BCL for each tile. The bufferSize only takes effects if the size of the buffer
     * (that stores the BCLs) is getting bigger than bufferSize
     * @param size is the new bufferSize
     */
    void setBufferSize(uint16_t size);

    /**
     * @return bufferSize
     */
    int getBufferSize();

    /**
     * Get BCL of all reads of a specific cycle where cycleOffset = 0 is the latest cycle
     * @param lane
     * @param tile
     * @param cycleOffset
     * @return the BCL as a vector<char>
     */
    const BCL &getBcl(uint16_t lane, uint16_t tile, uint16_t cycleOffset);

    /**
     * Returns the buffer of the BCLs for a given lane and tile
     * @param lane
     * @param tile
     * @return the buffer as a deque<vector<char>>
     */
    const std::deque<BCL> &getBcls(uint16_t lane, uint16_t tile);

    /**
     * Returns the most recent BCL of each tile.
     * This is eqivalent to `getBcl(lane, tile, 0)` for each tile
     * @param lane
     * @return deque of BCL, where each BCL belongs to a different tile
     */
    std::deque<BCL> getMostRecentBcls(uint16_t lane);

    /**
     * Returns all stored BCL data.
     * Keep in mind that is are not necessary all BCLs which have been parsed, because only the most recent BCLs are
     * stored
     * @return BCLDATA
     */
    BCLDATA& getBclData();

    /**
     * Adds the newBcl into the buffer of the specified tile.
     * If the size of the buffer is bigger than the bufferSize, the oldest BCL in this buffer is discarded
     * @param lane
     * @param tile
     * @param newBcl
     */
    void addBcl(uint16_t lane, uint16_t tile, BCL newBcl);
};


#endif //LIVEKIT_BCLREPRESENTATION_H
