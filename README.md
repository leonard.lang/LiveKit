# Genetic Live
## Installation with root permissions

### CMake
- Note that different Boost versions require different CMake versions

| Boost version   | CMake version  |
| :-----------:   | :------:       |
| 1.63            | 3.7 or newer   |
| 1.64            | 3.8 or newer   |        
| 1.65 and 1.65.1 | 3.9.3 or newer |
| 1.66            | 3.11 or newer  |
| 1.67            | 3.12 or newer  |
| 1.68 and 1.69   | 3.13 or newer  |

- If you have not already, install cmake (version >= 2.8)
```sudo apt-get install cmake```
- Check the cmake version with following command:
```cmake --version```
- Adapt your boost version to your cmake version or update cmake

### Zlib
```sudo apt-get install zlib1g-dev```

### Lz4
run the following command:
```
wget -c https://github.com/lz4/lz4/archive/v1.8.3.tar.gz && 
tar -xvzf v1.8.3.tar.gz &&
cd  lz4-1.8.3 &&
make &&
make install &&
cd ..
```
- Lz4 will be usually be installed in your /usr/local directory
- If this is not the case, you can change the path to Lz4 in the CMakeLists.txt under the section "Setup Zlib and lz4 libraries"

### Seqan
- run the following command
```
wget -c 'packages.seqan.de/seqan-src/seqan-src-2.3.2.tar.gz' &&
tar -xvzf seqan-src-2.3.2.tar.gz
```

- Adjust the /util/cmake-Path and the /include-Path in the "Setup seqan libraries"-Section in the CMakeLists.txt to the corresponding path:
[pathToSeqan]/seqan-seqan-v2.3.2/util/cmake and [pathToSeqan]/seqan-seqan-v2.3.2/include

### Boost

- build-essentials
```sudo apt-get install -y build-essential```
- boost.1.69.0 (system, filesystem, program_options)
- do the following steps:
  - create the file `user-config.jam` with the following content:
    ```
    ##### content of user-config.jam #####

        # Configure specific Python version.
        using python : 3.6 ; # Make both versions of Python available
        using python : 2.7 ; # To build with python 2.4, add python=2.4
                             # to your command line.

    ```
  - run the following command:
    ```
    wget -c 'http://sourceforge.net/projects/boost/files/boost/1.69.0/boost_1_69_0.tar.bz2/download' &&
    tar xf download &&
    cd boost_1_69_0 &&
    ./bootstrap.sh --with-python=/usr/bin/python3 &&
    mv ../user-config.jam ./user-config.jam &&
    sudo ./b2 cxxflags=-fPIC cflags=-fPIC -a install &&
    cd ..
    ```

### LiveKit Installation
Check out the LiveKit source code from the project website and make the project. We recommend to have separated source and build directories:
```
mkdir LiveKit && 
cd LiveKit &&
git clone https://gitlab.com/GeneticLife/LiveKit source &&
mkdir build && 
cd build &&
cmake ../source &&
make &&
cd ../..
```

# Local installation without root permissions

### CMake
- Note that different Boost versions require different CMake versions (see boost-version List above)

- If you have not already, install cmake (version >= 2.8)
```
wget -c https://github.com/Kitware/CMake/releases/download/v3.14.0/cmake-3.14.0.tar.gz && 
tar -xvzf cmake-3.14.0.tar.gz &&
cd cmake-3.14.0/ &&
./configure --prefix=**[pathOfYourChoice]** &&
make &&
make install &&
cd ..
```
- Check the cmake version with following command:
```
cd bin/ &&
cmake --version &&
cd ..
```

### Zlib
- run the following command:
```
wget http://www.zlib.net/zlib-1.2.11.tar.gz &&
tar -xvzf zlib-1.2.11.tar.gz &&
cd zlib-1.2.11 &&
./configure --prefix=[PathOfYourChoice]/zlib &&
make &&
make install &&
cd ..
```

### Lz4
run the following command:
```
wget -c https://github.com/lz4/lz4/archive/v1.8.3.tar.gz && 
tar -xvzf v1.8.3.tar.gz &&
cd  lz4-1.8.3 &&
make &&
make install PREFIX=[pathOfYourChoice]/lz4-1.8.3 &&
cd ..
```
- change the path in the CMakeLists to [pathOfYourChoice]/lz4-1.8.3/lib under the section "Setup Zlib and lz4 libraries"
- Delete the # at the beginning of the line

### Seqan
- run the following command:
```
wget -c 'packages.seqan.de/seqan-src/seqan-src-2.3.2.tar.gz' &&
tar -xvzf seqan-src-2.3.2.tar.gz
```
- Adjust the /util/cmake-Path and the /include-Path in the "Setup seqan libraries"-Section in the CMakeLists.txt to the corresponding path:
[pathToSeqan]/seqan-seqan-v2.3.2/util/cmake and [pathToSeqan]/seqan-seqan-v2.3.2/include

### Boost
- build-essentials
```sudo apt-get install -y build-essential```
- boost.1.69.0 (system, filesystem, program_options)
- do the following steps:
  - create the file `user-config.jam` with the following content:
    ```
    ##### content of user-config.jam #####

        # Configure specific Python version.
        using python : 3.6 ; # Make both versions of Python available
        using python : 2.7 ; # To build with python 2.4, add python=2.4
                             # to your command line.

    ```
  - run the following command:
    ```
    wget -c 'http://sourceforge.net/projects/boost/files/boost/1.69.0/boost_1_69_0.tar.bz2/boost' &&
    tar xf boost &&
    cd boost_1_69_0 &&
    ./bootstrap.sh --prefix=[PathOfYourChoice] --with-python=/usr/bin/python3 &&
    mv ../user-config.jam ./user-config.jam &&
    ./b2 cxxflags=-fPIC cflags=-fPIC -a install &&
    cd ..
    ```

### LiveKit Installation
- If you installed cmake without root permission you want to use this way 
- Check out the LiveKit source code from the project website and make the project. We recommend to have separated source and build directories:
```
mkdir LiveKit && 
cd LiveKit &&
git clone https://gitlab.com/GeneticLife/LiveKit.git source &&
mkdir build && 
cd build &&
[pathToCmake]/cmake ../source &&
make &&
cd ../..
```
## Additional Dependencies
- There might be additional dependencies for plugins
- Check the READMEs of the plugins to resolve potential dependencies


## Branch Prefixes

- design
- misc
- master
- develop
- feature
- bugfix
- refactoring
- test

## Additional Notes

- To achieve runtime performance, compile with *Release* - option enabled
- (change CMAKE_BUILD_TYPE in the CMakeLists.txt from *Debug* to *Release*)

